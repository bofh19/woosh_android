package in.w4rlock.woosh.fragments;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.service.Server;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.ServerServiceBinder;
import in.w4rlock.woosh.service.models.GetFriendsList;
import in.w4rlock.woosh.service.models.PingOperation;
import in.w4rlock.woosh.service.models.SocketMessage;
import in.w4rlock.woosh.utils.OauthUtils;
import in.w4rlock.woosh.utils.Utils;

public class LoginFragment extends Fragment implements OauthUtils.ActivityCallback{
    private static final String TAG = "LoginFragment";
    private final Handler mHandler = new Handler();
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG,"attach to service handler called "+mBound);
            if (!mBound) {
                attachToService();
            }
        }
    };

    public LoginFragment() {
    }

    private TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login_old, container, false);
        textView = (TextView) rootView.findViewById(R.id.hello_world);
        Button b = (Button) rootView.findViewById(R.id.button);

        Intent serviceIntent = new Intent(ServerService.START_SERVICE);
        serviceIntent.setPackage(ServerService.PACKAGE_NAME);
        getActivity().startService(serviceIntent);
        mHandler.postDelayed(mAttachToService, 500);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PingOperation socketMessage = new PingOperation();
                List<SocketMessage> socketMessages = new ArrayList<>();
                socketMessages.add(socketMessage);
                if (mServerService != null)
                    mServerService.writeMessage(socketMessages);
            }
        });

        getActivity().getApplicationContext().registerReceiver(changeTextInputThrouReceiver, new IntentFilter("PlayNextSong"));

        Button b2 = (Button) rootView.findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingIntent contentIntent = PendingIntent.getBroadcast(getActivity(), 0, new Intent("PlayNextSong"),
                        PendingIntent.FLAG_UPDATE_CURRENT);
                try {
                    contentIntent.send();
                } catch (PendingIntent.CanceledException e) {
                    Log.d(TAG,"failed on pending intent "+e.toString());
                }
            }
        });
        btnSignIn = (SignInButton) rootView.findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAuthProcess();
            }
        });
        ((Button)rootView.findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiKeyControllder.clearApiKey(getActivity().getApplicationContext());
                mEmail = null;
            }
        });

        rootView.findViewById(R.id.req_friends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetFriendsList socketMessage = new GetFriendsList();
                List<SocketMessage> socketMessages = new ArrayList<>();
                socketMessages.add(socketMessage);
                if (mServerService != null)
                    mServerService.writeMessage(socketMessages);
            }
        });

        rootView.findViewById(R.id.hash_encode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hashEncode();
            }
        });
        rootView.findViewById(R.id.hash_decode).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hashDecode();
            }
        });
        rootView.findViewById(R.id.hash_compare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, " compare: " + hashCompare());
            }
        });
        img = (ImageView) rootView.findViewById(R.id.image_view);

        return rootView;
    }

    private static TestData original;
    private static String b64Encoded = "";
    private static TestData b64Decoded;
    ImageView img;
    public static class TestData {
        public String some_text;
        public String some_image_text;
    }

    public void hashEncode(){
        Log.d(TAG,"hash Encode");
        original = new TestData();
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.android);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        largeIcon.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        original.some_image_text = Base64.encodeToString(byteArray, Base64.DEFAULT);
        original.some_text = "this is a test text";

        Gson gson = new Gson();
        Type listType = new TypeToken<TestData>() {}.getType();
        String out = gson.toJson(original, listType);

        b64Encoded = Base64.encodeToString(out.getBytes(), Base64.DEFAULT);

        Log.d(TAG, "encoded: "+b64Encoded);
    }

    public void hashDecode(){
        Log.d(TAG, "hash Decode");
        String decodedJson = new String(Base64.decode(b64Encoded, Base64.DEFAULT));
        Gson gson = new Gson();
        Type listType = new TypeToken<TestData>() {}.getType();
        b64Decoded = gson.fromJson(decodedJson,listType);
    }

    public boolean hashCompare(){
        Log.d(TAG,"hash Compare");
        if(original != null && b64Decoded != null && b64Encoded.length() > 0 &&
                original.some_text.equals(b64Decoded.some_text) &&
                original.some_image_text.equals(b64Decoded.some_image_text)){

            byte[] imageBytes = Base64.decode(b64Decoded.some_image_text, Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageBytes,0,imageBytes.length);
            img.setImageBitmap(bmp);

            return true;
        }
        return false;
    }

    protected BroadcastReceiver changeTextInputThrouReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            textView.setText("recieved broadcast event");
        }
    };

    ServerService mServerService;
    boolean mBound = false;

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        detachFromService();
    }

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isServerServiceRunning(getActivity())) {
            // binding only if server service is already running
            Intent intent = new Intent(getActivity(), ServerService.class);
            getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mHandler.removeCallbacks(mAttachToService);
        } else {
            // we have to start it and get binded
            Log.d(TAG,"service not running starting and adding callback");
            Intent serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            getActivity().startService(serviceIntent);
            mHandler.postDelayed(mAttachToService,500);
        }
    }

    private void detachFromService() {
        // Unbind from the service
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
        mServerService = null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            // remove callbacks here probably
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            Log.d(TAG,"service connected");
            ServerServiceBinder binder = (ServerServiceBinder) service;
            mServerService = binder.getService();
            mBound = true;
        }
    };


    public void startAuthProcess() {
        if (ApiKeyControllder.hasKey(getActivity().getApplicationContext())) {
            Log.d(TAG, ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
            Utils.showShortToast(getActivity().getApplicationContext(), ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
        } else {
            getUsername();
        }
    }

    private void getUsername() {
        if (mEmail == null) {
            pickUserAccount();
        } else {
            new OauthUtils.GetUsernameTask(this, mEmail, SCOPE).execute();
        }
    }

    private void pickUserAccount() {
        Log.d(TAG,"pickuseraccount");
        String[] accountTypes = new String[] { "com.google" };
        Log.d(TAG,"account types");
        Intent intent = AccountPicker.newChooseAccountIntent(null, null, accountTypes, true, null, null, null, null);
        Log.d(TAG,"intent created");
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
        Log.d(TAG,"activity started");
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"onactivityresult: "+requestCode + " resultcode: "+resultCode);
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            // Receiving a result from the AccountPicker
            if (resultCode == Activity.RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                // With the account name acquired, go get the auth token
                getUsername();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The account picker dialog closed without selecting an
                // account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(getActivity(), "pick account", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR && resultCode == Activity.RESULT_OK) {
            // Receiving a result that follows a GoogleAuthException, try auth
            // again
            getUsername();
        }
    }

    String mEmail; // Received from newChooseAccountIntent(); passed to
    // getToken()
    private static final String SCOPE = "oauth2:email https://www.google.com/m8/feeds/";
    private SignInButton btnSignIn;
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;


    @Override
    public void onLoadingStarted() {

    }

    @Override
    public void onLoadingEnded(String token) {
//        if(logout){
//            OauthUtils.ClearToken clearToken = new OauthUtils.ClearToken(getActivity().getApplicationContext(), token);
//            clearToken.execute();
//            return;
//        }
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        ApiKeyControllder.Callback callback = new ApiKeyControllder.Callback() {

            @Override
            public void onStart() {
                dialog.show();
                Log.d(TAG, "loading started");
            }

            @Override
            public void onFailed() {
                dialog.dismiss();
                Log.d(TAG, "loading failed");
                Utils.showShortToast(getActivity().getApplicationContext(), "failed");
            }

            @Override
            public void onEnd() {
                dialog.dismiss();
                Log.d(TAG, "loading ended");
                Utils.showShortToast(getActivity().getApplicationContext(), ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
                Server.apiKey = ApiKeyControllder.getApiKey(getActivity().getApplicationContext());
            }
        };
        if (token != null) {
            ApiKeyControllder.fetchAndSaveApiKey(token, callback, getActivity().getApplicationContext());
        }
    }

    @Override
    public void handleException(final UserRecoverableAuthException userRecoverableAuthException) {
        // Because this call comes from the AsyncTask, we must ensure that the
        // following
        // code instead executes on the UI thread.
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (userRecoverableAuthException instanceof GooglePlayServicesAvailabilityException) {
                    // The Google Play services APK is old, disabled, or not
                    // present.
                    // Show a dialog created by Google Play services that allows
                    // the user to update the APK
                    int statusCode = ((GooglePlayServicesAvailabilityException) userRecoverableAuthException).getConnectionStatusCode();
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode, getActivity(), REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    dialog.show();
                } else if (userRecoverableAuthException instanceof UserRecoverableAuthException) {
                    // Unable to authenticate, such as when the user has not yet
                    // granted
                    // the app access to the account, but the user can fix this.
                    // Forward the user to an activity in Google Play services.
                    Intent intent = ((UserRecoverableAuthException) userRecoverableAuthException).getIntent();
                    startActivityForResult(intent, REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                }
            }
        });
    }
}