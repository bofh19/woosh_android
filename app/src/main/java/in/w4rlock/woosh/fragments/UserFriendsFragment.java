package in.w4rlock.woosh.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.util.Util;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import in.w4rlock.woosh.ChatWithXActivity;
import in.w4rlock.woosh.PendingUserFriendActivity;
import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.ChatListRecyclerAdapter;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.DbService;
import in.w4rlock.woosh.database.db.DbServiceInt;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.models.ApiKey;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class UserFriendsFragment extends BaseFragment {
    private static final String ARG_DUMMY = "arg_dummy";
    private String mDummy;
    private static final String TAG = "UserFriendsFragment";

    private ChatListRecyclerAdapter userFriendsViewAdapter;
    private RecyclerView userFriendsView;
    private View rootView;
    private String addFriendScannedToken = null;

    public static UserFriendsFragment newInstance() {
        UserFriendsFragment fragment = new UserFriendsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DUMMY, "dummy");
        fragment.setArguments(args);
        return fragment;
    }

    public UserFriendsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mDummy = getArguments().getString(ARG_DUMMY);
        }
    }

    @Override
    public void onResume() {
        L.d(TAG, "onResume");
        super.onResume();
        getAndSetFriendsFromDb();
    }

    @Override
    public void onPause(){
        L.d(TAG, "OnPauseCalledUserFriendsFragment");
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String toast = "";
        if(result != null) {
            if(result.getContents() == null) {
                Utils.showSnackbar(rootView, "Unable to add user friend, got no key");
            } else {
                addFriendScannedToken = result.getContents();
                L.d(TAG, "scannedString: "+addFriendScannedToken);
                if(mServerHelper != null){
                    mServerHelper.onAddUserFriendFromToken(addFriendScannedToken);
                    Utils.showShortToast(getContext(), "Adding user friend");
                    addFriendScannedToken = null;
                }else{
                    getService();
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_user_friends_tab, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.user_friends_refresh:
                reloadUserFriendsFromServer();
                break;
            case R.id.user_friends_add:
                addUserFriend();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addUserFriend() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View promptsView = inflater.inflate(R.layout.fragment_user_friends_add_user_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(promptsView);
        final EditText userNameOrEmail = (EditText) promptsView.findViewById(R.id.frag_user_friends_add_user_email_username_edt);
        final TextView validationText = (TextView) promptsView.findViewById(R.id.frag_user_friends_add_user_valid_value) ;
        final Button scanButton = (Button) promptsView.findViewById(R.id.frag_user_friends_add_user_scan);
        alertDialogBuilder.setCancelable(false).setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               //doing nothing here
            }
        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        L.d(TAG, "clicked on ok: " + userNameOrEmail.getText());
                        String userVal = userNameOrEmail.getText().toString();
                        if(userVal.isEmpty()){
                            validationText.setVisibility(View.VISIBLE);
                        }else{
                            mServerHelper.addUserFriend(userVal, null);
                            dialog.dismiss();
                        }
                    }
                });

                scanButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addFriendFromScan();
                        dialog.dismiss();
                    }
                });
            }
        });

        alertDialog.show();
    }

    private void addFriendFromScan(){
        IntentIntegrator.forSupportFragment(this).initiateScan();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_user_friends, container, false);
        userFriendsView = (RecyclerView) rootView.findViewById(R.id.user_friends_view);
        userFriendsView.setLayoutManager(new LinearLayoutManager(userFriendsView.getContext()));
        userFriendsViewAdapter = new ChatListRecyclerAdapter(getActivity().getApplicationContext(), mRecycleViewCallback);
        userFriendsView.setAdapter(userFriendsViewAdapter);
        getAndSetFriendsFromDb();
        return rootView;
    }

    ChatListRecyclerAdapter.RecyclerItemSelected mRecycleViewCallback = new ChatListRecyclerAdapter.RecyclerItemSelected() {
        @Override
        public void onItemSelected(UserFriend friend) {
            if(!friend.secretKeyExchanged){
                //if you are the one who invited and key is not exchanged then start the barcode
                Intent i = new Intent(UserFriendsFragment.this.getActivity(), PendingUserFriendActivity.class);
                DbServiceInt dbService = new DbService(getContext());
                String secretKey = dbService.getUserSecretKey(friend);
                L.d(TAG, "FriendAuthKey: "+friend.auth_key);
                L.d(TAG, "FriendSecretKey: " + secretKey);
                secretKey = friend.auth_key + ":" + secretKey;
                i.putExtra(PendingUserFriendActivity.AUTH_KEY, secretKey);
                getActivity().startActivity(i);
            }else{
                //else check if the friend has accepted you and start the chat
                if(friend.accepted) {
                    ChatFragment.chatingWithId = friend.id;
                    Intent i = new Intent(UserFriendsFragment.this.getActivity(), ChatWithXActivity.class);
                    i.putExtra(ChatWithXActivity.CHAT_WITH_ID, friend.id);
                    getActivity().startActivity(i);
                }
            }
        }
    };

    class ServerHelperCallback extends ServerHelperCallbackAdapter {
        @Override
        public void dbChanged(DBChanged changeId) {
            if (changeId == DBChanged.UserFriendsReceived) {
                getAndSetFriendsFromDb();
            }
        }

        @Override
        public  void onAddUserFriendResponse(AddUserFriend addUserFriend){
            if( !addUserFriend.getAddUserError().isEmpty()  || addUserFriend.getAuthKey().isEmpty()  ){
                Utils.showSnackbar( rootView, addUserFriend.getAddUserError() );
            }else{
                Utils.showSnackbar(rootView, getContext().getString(R.string.friend_request_pending));
            }
        }
    }

    ServerHelperCallback serverCallback = new ServerHelperCallback();

    private void getAndSetFriendsFromDb() {
        if (getActivity() == null)
            return;
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(getActivity().getApplicationContext());
        Cursor userFriends = cupboard().withDatabase(db).query(UserFriend.class).getCursor();
        QueryResultIterable<UserFriend> itr = cupboard().withCursor(userFriends).iterate(UserFriend.class);
        final ArrayList<UserFriend> userFriendsFinal = new ArrayList<>();
        for (UserFriend friend : itr) {
            userFriendsFinal.add(friend);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                userFriendsViewAdapter.addAllItems(userFriendsFinal);
            }
        });
        db.close();
    }

    private void reloadUserFriendsFromServer() {
        if (mServerHelper != null) {
            mServerHelper.getFriendsFromServer();
        } else {
            super.getService();
        }
    }

    @Override
    public void onConnectedToServer() {
        L.d(TAG, "onConnectedToServer");
        reloadUserFriendsFromServer();
        if(addFriendScannedToken != null){
            mServerHelper.onAddUserFriendFromToken(addFriendScannedToken);
            addFriendScannedToken = null;
            Utils.showShortToast(getContext(), "Adding user friend");
        }
    }

    @Override
    public ServerHelperCallbackInterface getServerCallback() {
        return serverCallback;
    }
}
