package in.w4rlock.woosh.fragments;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;

import in.w4rlock.woosh.MainActivity;
import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.service.Server;
import in.w4rlock.woosh.utils.OauthUtils;
import in.w4rlock.woosh.utils.Utils;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class LoginActivityFragment extends Fragment implements OauthUtils.ActivityCallback {
    private static final String TAG = "LoginActivityFragment";
    public LoginActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        btnSignIn = (SignInButton) rootView.findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiKeyControllder.clearApiKey(getActivity().getApplicationContext());
                mEmail = null;
                startAuthProcess();
            }
        });
        return rootView;
    }

    public void startAuthProcess() {
        if (ApiKeyControllder.hasKey(getActivity().getApplicationContext())) {
            Log.d(TAG, ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
            Utils.showShortToast(getActivity().getApplicationContext(), ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
        } else {
            getUsername();
        }
    }

    private void getUsername() {
        if (mEmail == null) {
            pickUserAccount();
        } else {
            new OauthUtils.GetUsernameTask(this, mEmail, SCOPE).execute();
        }
    }

    private void pickUserAccount() {
        Log.d(TAG,"pickuseraccount");
        String[] accountTypes = new String[] { "com.google" };
        Log.d(TAG,"account types");
        Intent intent = AccountPicker.newChooseAccountIntent(null, null, accountTypes, true, null, null, null, null);
        Log.d(TAG,"intent created");
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
        Log.d(TAG,"activity started");
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"onactivityresult: "+requestCode + " resultcode: "+resultCode);
        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            // Receiving a result from the AccountPicker
            if (resultCode == Activity.RESULT_OK) {
                mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                // With the account name acquired, go get the auth token
                getUsername();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The account picker dialog closed without selecting an
                // account.
                // Notify users that they must pick an account to proceed.
                Toast.makeText(getActivity(), "pick account", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR && resultCode == Activity.RESULT_OK) {
            // Receiving a result that follows a GoogleAuthException, try auth
            // again
            getUsername();
        }
    }

    String mEmail; // Received from newChooseAccountIntent(); passed to
    // getToken()
    private static final String SCOPE = "oauth2:email https://www.google.com/m8/feeds/";
    private SignInButton btnSignIn;
    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;


    @Override
    public void onLoadingStarted() {

    }

    @Override
    public void onLoadingEnded(String token) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        dialog.setCancelable(false);
        ApiKeyControllder.Callback callback = new ApiKeyControllder.Callback() {

            @Override
            public void onStart() {
                dialog.show();
                Log.d(TAG, "loading started");
            }

            @Override
            public void onFailed() {
                dialog.dismiss();
                Log.d(TAG, "loading failed");
                Utils.showShortToast(getActivity().getApplicationContext(), "failed");
            }

            @Override
            public void onEnd() {
                dialog.dismiss();
                Log.d(TAG, "loading ended");
//                Utils.showShortToast(getActivity().getApplicationContext(), ApiKeyControllder.getApiKey(getActivity().getApplicationContext()));
                Server.apiKey = ApiKeyControllder.getApiKey(getActivity().getApplicationContext());
                if(Server.apiKey != null){
                    Intent main = new Intent(getActivity(), MainActivity.class);
                    main.putExtra(MainActivity.FROM_LOGIN,true);
                    getActivity().startActivity(main);
                    getActivity().finish();
                }
            }
        };
        if (token != null) {
            ApiKeyControllder.fetchAndSaveApiKey(token, callback, getActivity().getApplicationContext());
        }
    }

    @Override
    public void handleException(final UserRecoverableAuthException userRecoverableAuthException) {
        // Because this call comes from the AsyncTask, we must ensure that the
        // following
        // code instead executes on the UI thread.
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (userRecoverableAuthException instanceof GooglePlayServicesAvailabilityException) {
                    // The Google Play services APK is old, disabled, or not
                    // present.
                    // Show a dialog created by Google Play services that allows
                    // the user to update the APK
                    int statusCode = ((GooglePlayServicesAvailabilityException) userRecoverableAuthException).getConnectionStatusCode();
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode, getActivity(), REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    dialog.show();
                } else if (userRecoverableAuthException instanceof UserRecoverableAuthException) {
                    // Unable to authenticate, such as when the user has not yet
                    // granted
                    // the app access to the account, but the user can fix this.
                    // Forward the user to an activity in Google Play services.
                    Intent intent = ((UserRecoverableAuthException) userRecoverableAuthException).getIntent();
                    startActivityForResult(intent, REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                }
            }
        });
    }
}
