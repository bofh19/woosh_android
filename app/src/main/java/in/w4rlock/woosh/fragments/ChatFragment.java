package in.w4rlock.woosh.fragments;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.MessageRecyclerAdapter;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.Server;
import in.w4rlock.woosh.service.ServerHelper;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.ServerHelperInterface;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.ServerServiceBinder;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.utils.Utils;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class ChatFragment extends Fragment {

    public static int chatingWithId = -1;
    private static final String TAG = "ChatFragment";
    private final Handler mHandler = new Handler();
    ServerService mServerService;
    boolean mBound = false;
    private ServerHelper mServerHelper;
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "attach to service handler called " + mBound);
            if (!mBound) {
                attachToService();
            }
        }
    };

    public ChatFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private MessageRecyclerAdapter mMessageRecyclerAdapter;
    private RecyclerView mChatRecyclerView;
    private LinearLayoutManager layoutManager;
    private String userId;
    private String chatId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        final EditText editText = (EditText)rootView.findViewById(R.id.editText);
        rootView.findViewById(R.id.send_msg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"sending message to user: "+chatingWithId);
                mServerHelper.sendUserMessageToServer(""+chatingWithId,editText.getText().toString(),null);
            }
        });

        mChatRecyclerView = (RecyclerView) rootView.findViewById(R.id.mMessages);
        mChatRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        mChatRecyclerView.setLayoutManager(layoutManager);

        userId = ApiKeyControllder.getId(getActivity().getApplicationContext());
        mMessageRecyclerAdapter = new MessageRecyclerAdapter(userId, getActivity().getApplicationContext());

        mChatRecyclerView.setAdapter(mMessageRecyclerAdapter);


        return rootView;
    }

    class ServerHelperCallback extends ServerHelperCallbackAdapter{
        @Override
        public void dbChanged(DBChanged changeId) {
            if(changeId == DBChanged.UserMessageReceived || changeId == DBChanged.UserMessageSent) {
                Log.d(TAG, "dbChanged callback");
                SQLiteDatabase db = SocketMessageDb.getReadDatabase(getActivity().getApplicationContext());
                Cursor userMessages = cupboard().withDatabase(db).query(UserMessage.class).withSelection("((toId = ? AND fromId = ?) OR (toId = ? AND fromId = ?))", userId, chatId, chatId, userId).getCursor();
                QueryResultIterable<UserMessage> itr = cupboard().withCursor(userMessages).iterate(UserMessage.class);
                final ArrayList<UserMessage> userMessages1 = new ArrayList<>();
                for (final UserMessage userMessage : itr) {
                    userMessages1.add(userMessage);
                }
                Log.d(TAG, "" + userMessages1);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageRecyclerAdapter.addAllItems(userMessages1);
                        mChatRecyclerView.scrollToPosition(userMessages1.size() - 1);
                    }
                });
                db.close();
            }
        }
    }

    ServerHelperCallback serverCallback = new ServerHelperCallback();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        attachToService();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
        detachFromService();
    }

    private void attachToService() {
        if (Utils.isServerServiceRunning(getActivity())) {
            Intent intent = new Intent(getActivity(), ServerService.class);
            getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mHandler.removeCallbacks(mAttachToService);
        } else {
            Log.d(TAG,"service not running starting and adding callback");
            Intent serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            getActivity().startService(serviceIntent);
            mHandler.postDelayed(mAttachToService,500);
        }
    }

    private void detachFromService() {
        mServerHelper.removeCallback(serverCallback);
        if (mBound) {
            getActivity().unbindService(mConnection);
            mBound = false;
        }
        mServerService = null;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            // remove callbacks here probably
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            Log.d(TAG,"service connected");
            ServerServiceBinder binder = (ServerServiceBinder) service;
            mServerService = binder.getService();
            mBound = true;
            mServerHelper = mServerService.getServerHelper();
            // attach callbacks here
            mServerHelper.addCallback(serverCallback);
        }
    };

}
