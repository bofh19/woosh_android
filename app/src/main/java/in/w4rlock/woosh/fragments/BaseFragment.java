package in.w4rlock.woosh.fragments;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;

import in.w4rlock.woosh.interfaces.OnFragmentInteractionListener;
import in.w4rlock.woosh.service.ServerHelper;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/9/15.
 */
public abstract class BaseFragment extends Fragment {
    private ServerService mServerService;
    protected ServerHelper mServerHelper;
    private OnFragmentInteractionListener mListener;
    private static final String TAG = "BaseFragment";

    public abstract ServerHelperCallbackInterface getServerCallback();

    /*
    activity methods
     */

    @Override
    public void onPause() {
        super.onPause();
        removeService();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "BaseFragment - onResume");
        super.onResume();
        if (mServerService != null || mServerHelper != null)
            getService();
    }

    private void removeService(){
        if(mServerHelper != null){
            mServerHelper.removeCallback(getServerCallback());
            callbackAdded = false;
        }
        mServerService = null;
        mServerHelper = null;
    }

    private final Handler mHandler = new Handler();
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "attach to service handler called " + mServerService);
            if (mServerService == null) {
                getService();
            }
        }
    };
    int tryCounter = 0;
    static final Object serverLockObj = new Object();
    private boolean callbackAdded = false;
    protected final void getService(){
        synchronized (serverLockObj){
            if(tryCounter >= 10)
                return;
            tryCounter++;
            mServerService = mListener.getServerService();
            if(mServerService != null) {
                if (mServerHelper == null)
                    mServerHelper = mServerService.getServerHelper();
                if(!callbackAdded){
                    mServerHelper.addCallback(getServerCallback());
                    callbackAdded = true;
                }
                tryCounter = 0; // reset try counter, preparing for next disconnect
                onConnectedToServer();
            }else{
                mHandler.postDelayed(mAttachToService,500);
            }
        }
    }

    public void onConnectedToServer() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            getService();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()  + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        removeService();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG,"setUserVisibleHint: "+isVisibleToUser);
        if (isVisibleToUser) {
            if (mServerService == null || mServerHelper == null)
                getService();
        }
        else {
        }
    }
}
