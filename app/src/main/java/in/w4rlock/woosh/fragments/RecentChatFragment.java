package in.w4rlock.woosh.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.w4rlock.woosh.ChatWithXActivity;
import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.ChatListRecyclerAdapter;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.ServerHelperInterface;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.utils.L;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;


/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class RecentChatFragment extends BaseFragment {
    private static final String ARG_DUMMY = "arg_dummy";
    private String mDummy;
    private static final String TAG="RecentChatFragment";

    public static ArrayList<UserFriend> friends = new ArrayList<>();
    private ChatListRecyclerAdapter recentChatFriendsViewAdapter;
    private RecyclerView recentChatFriendsView;

    public static RecentChatFragment newInstance() {
        RecentChatFragment fragment = new RecentChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DUMMY, "dummy");
        fragment.setArguments(args);
        return fragment;
    }

    public RecentChatFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDummy = getArguments().getString(ARG_DUMMY);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        setRecentChatUsers();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recent_chat, container, false);
        recentChatFriendsView = (RecyclerView)rootView.findViewById(R.id.recent_chat_view);
        recentChatFriendsView.setLayoutManager(new LinearLayoutManager(recentChatFriendsView.getContext()));
        recentChatFriendsViewAdapter = new ChatListRecyclerAdapter(getActivity().getApplicationContext(), mRecycleViewCallback);

        recentChatFriendsView.setAdapter(recentChatFriendsViewAdapter);

//        Cursor cr = cupboard().withDatabase(SocketMessageDb.getReadDatabase(getActivity())).query(UserMessage.class).getCursor();
//        cr.registerDataSetObserver(new DataSetObserver() {
//            @Override
//            public void onChanged() {
//                L.d(TAG,"UserMessage: data set observer onChanged");
//            }
//
//            @Override
//            public void onInvalidated() {
//                L.d(TAG, "UserMessage: data set observer onInvalidated");
//            }
//        });


//        setRecentChatUsers();
        return rootView;
    }

    private void setRecentChatUsers() {
        ArrayList<UserFriend> friends = new ArrayList<>();
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(getActivity().getApplicationContext());
        Cursor userFriends = cupboard().withDatabase(db).query(UserFriend.class).getCursor();
        QueryResultIterable<UserFriend> itr = cupboard().withCursor(userFriends).iterate(UserFriend.class);
        for(UserFriend friend: itr){
            friends.add(friend);
        }
        recentChatFriendsViewAdapter.addAllItems(friends);
        db.close();
    }

    ChatListRecyclerAdapter.RecyclerItemSelected mRecycleViewCallback  = new ChatListRecyclerAdapter.RecyclerItemSelected() {
        @Override
        public void onItemSelected(UserFriend friend) {
            ChatFragment.chatingWithId = friend.id;
            Intent i = new Intent(RecentChatFragment.this.getActivity(), ChatWithXActivity.class);
            i.putExtra(ChatWithXActivity.CHAT_WITH_ID, friend.id);
            getActivity().startActivity(i);
        }
    };

    class ServerHelperCallback extends ServerHelperCallbackAdapter {
        @Override
        public void dbChanged(DBChanged changeId) {
            if(changeId == DBChanged.UserMessageReceived || changeId == DBChanged.UserMessageSent) {
                L.d(TAG, ""+changeId);
                RecentChatFragment.this.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRecentChatUsers();
                    }
                });
            }
        }
    }

    ServerHelperCallback serverCallback = new ServerHelperCallback();

    @Override
    public ServerHelperCallbackInterface getServerCallback() {
        return serverCallback;
    }
}
