package in.w4rlock.woosh.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.MessageRecyclerAdapter;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.ServerHelperInterface;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.utils.L;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * A placeholder fragment containing a simple view.
 */
public class ChatWithXFragment extends BaseFragment {
    private static final String ARG_CHAT_WITH_ID = "arg_chat_with-id";
    private int chatId;
    private static final String TAG = "ChatWithXFragment";

    private MessageRecyclerAdapter mMessageRecyclerAdapter;
    private RecyclerView mChatRecyclerView;
    private LinearLayoutManager layoutManager;
    private String userId;
    private UserFriend userFriend;

    public static ChatWithXFragment newInstance(int chatId) {
        ChatWithXFragment fragment = new ChatWithXFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CHAT_WITH_ID, chatId);
        fragment.setArguments(args);
        return fragment;
    }

    public ChatWithXFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            chatId = getArguments().getInt(ARG_CHAT_WITH_ID);
        }
        Log.d(TAG,""+chatId);

        SQLiteDatabase db = SocketMessageDb.getReadDatabase(getActivity().getApplicationContext());
        userFriend = cupboard().withDatabase(db).query(UserFriend.class).withSelection(" id = ?", "" + chatId).get();
        userId = ApiKeyControllder.getId(getActivity().getApplicationContext());
        Log.d(TAG,"userFriend: "+userFriend);
        Log.d(TAG,"userFriendPrinted");
        if (userFriend == null){
            getActivity().finish();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat_with_x, container, false);

        final EditText editText = (EditText)rootView.findViewById(R.id.editText);
        rootView.findViewById(R.id.send_msg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"sending message to user: "+chatId);
                try {
                    final UserMessage userMessage = new UserMessage(editText.getText().toString(), "" + chatId, null, getActivity().getApplicationContext());
                    if(mMessageRecyclerAdapter != null){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMessageRecyclerAdapter.addItem(userMessage);
                            }
                        });
                    }
                }catch (Exception e){
                    L.d(TAG,"exception while getting a temp user message: "+e.toString());
                }
                mServerHelper.sendUserMessageToServer(""+chatId,editText.getText().toString(),null);
                editText.setText("");

            }
        });

        mChatRecyclerView = (RecyclerView) rootView.findViewById(R.id.mMessages);
        mChatRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        mChatRecyclerView.setLayoutManager(layoutManager);
        mChatRecyclerView.addOnScrollListener(mScrollListener);
        userId = ApiKeyControllder.getId(getActivity().getApplicationContext());
        mMessageRecyclerAdapter = new MessageRecyclerAdapter(userId, getActivity().getApplicationContext());

        mChatRecyclerView.setAdapter(mMessageRecyclerAdapter);

        getAndSetMessagesFromDb();
        return rootView;
    }

    class ServerHelperCallback extends ServerHelperCallbackAdapter {
        @Override
        public void dbChanged(DBChanged changeId) {
//            if(changeId == DBChanged.UserMessageReceived || changeId == DBChanged.UserMessageSent || changeId == DBChanged.UserMessageSentRecv) {
//                Log.d(TAG, "dbChanged callback");
//                getAndSetMessagesFromDb();
//            }
        }

        public void onSendUserMessage(final UserMessage userMessage){
            if(userMessage.getToId().equals(""+chatId)){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageRecyclerAdapter.addItem(userMessage);
                    }
                });
            }
        }

        public void onUserMessage(final UserMessage userMessage){
            if(userMessage.getToId().equals(userId)){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageRecyclerAdapter.addItem(userMessage);
                    }
                });
            }
        }

        public void onUserMessageSent(UserMessageSent userMessageSent){
            mMessageRecyclerAdapter.updateUserMessage(userMessageSent);
        }

        public void onUserMessageReceived(UserMessageReceived userMessageReceived){
            mMessageRecyclerAdapter.updateUserMessage(userMessageReceived);
        }
    }
    ServerHelperCallback serverCallback = new ServerHelperCallback();

    RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            L.d(TAG,"newState: "+newState);
        }
    };

    private void getAndSetMessagesFromDb(){
        String chatId = ""+this.chatId;
        GetMessagesAsyncTask task = new GetMessagesAsyncTask();
        task.execute(userId, chatId);
    }

    class GetMessagesAsyncTask extends AsyncTask<String,Void,ArrayList<UserMessage>>{

        @Override
        protected ArrayList<UserMessage> doInBackground(String... params) {
            Log.d(TAG, "inside get messages async task");
            String userId = params[0];
            String chatId = params[1];
            SQLiteDatabase db = SocketMessageDb.getReadDatabase(getActivity().getApplicationContext());
            Cursor userMessages = cupboard().withDatabase(db).query(UserMessage.class).withSelection("((toId = ? AND fromId = ?) OR (toId = ? AND fromId = ?))", userId, chatId, chatId, userId).getCursor();
            QueryResultIterable<UserMessage> itr = cupboard().withCursor(userMessages).iterate(UserMessage.class);
            final ArrayList<UserMessage> userMessages1 = new ArrayList<>();
            for (final UserMessage userMessage : itr) {
                if(userMessage == null){
                    L.d(TAG, "user message is null wtf ?????");
                }else{
                    L.d(TAG,"trying to print user message");
                    L.d(TAG, ""+userMessage);
                }
                userMessages1.add(userMessage);
            }
            //Log.d(TAG, "" + userMessages1);
            db.close();
            return userMessages1;
        }

        @Override
        protected void onPostExecute(final ArrayList<UserMessage> result){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMessageRecyclerAdapter.addAllItems(result);
                    mChatRecyclerView.scrollToPosition(result.size() - 1);
                }
            });
        }
    }

    @Override
    public ServerHelperCallbackInterface getServerCallback() {
        return serverCallback;
    }
}
