package in.w4rlock.woosh.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.ContactsListRecyclerAdapter;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.utils.L;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class UserContactsFragment extends BaseFragment {
    private static final String ARG_DUMMY = "arg_dummy";
    private String mDummy;

    private static final String TAG="UserContactsFragment";

    private ContactsListRecyclerAdapter userContactsAdapter;
    private RecyclerView userContactsView;

    public static UserContactsFragment newInstance() {
        UserContactsFragment fragment = new UserContactsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DUMMY, "dummy");
        fragment.setArguments(args);
        return fragment;
    }

    public UserContactsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mDummy = getArguments().getString(ARG_DUMMY);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getAndSetFriendsFromDb();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_user_contacts_tab, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.user_contacts_refresh:
                reloadUserFriendsFromServer();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_friends, container, false);
        userContactsView = (RecyclerView)rootView.findViewById(R.id.user_friends_view);
        userContactsView.setLayoutManager(new LinearLayoutManager(userContactsView.getContext()));
        userContactsAdapter = new ContactsListRecyclerAdapter(getActivity().getApplicationContext(), mRecycleViewCallback);
        userContactsView.setAdapter(userContactsAdapter);
        getAndSetFriendsFromDb();
        return rootView;
    }

    ContactsListRecyclerAdapter.RecyclerItemSelected mRecycleViewCallback  = new ContactsListRecyclerAdapter.RecyclerItemSelected() {
        @Override
        public void onItemSelected(UserContact contact) {
        }
    };

    class ServerHelperCallback extends ServerHelperCallbackAdapter {
        @Override
        public void dbChanged(DBChanged changeId) {
            if(changeId == DBChanged.UserContactsReceived){
                getAndSetFriendsFromDb();
            }
        }
    }
    ServerHelperCallback serverCallback = new ServerHelperCallback();

    private void getAndSetFriendsFromDb(){
        if(getActivity() == null)
            return;
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(getActivity().getApplicationContext());
        Cursor userContacts = cupboard().withDatabase(db).query(UserContact.class).getCursor();
        QueryResultIterable<UserContact> itr = cupboard().withCursor(userContacts).iterate(UserContact.class);
        final ArrayList<UserContact> userContactsFinal = new ArrayList<>();
        for(UserContact contact: itr){
            userContactsFinal.add(contact);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                userContactsAdapter.addAllItems(userContactsFinal);
            }
        });
        db.close();
    }

    private void reloadUserFriendsFromServer(){
        if (mServerHelper != null) {
            Log.d(TAG,"not null");
            mServerHelper.getContactsFromServer();
        }
        else{
            super.getService();
        }
    }

    @Override
    public void onConnectedToServer() {
        L.d(TAG,"doing nothing as it is a long shitty task");
    }

    @Override
    public ServerHelperCallbackInterface getServerCallback() {
        return serverCallback;
    }

}
