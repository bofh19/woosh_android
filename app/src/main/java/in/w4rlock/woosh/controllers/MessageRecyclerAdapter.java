package in.w4rlock.woosh.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;

public class MessageRecyclerAdapter extends RecyclerView.Adapter<MessageRecyclerAdapter.MessageTextViewHolder> {
    private static final String TAG = "MessageRecyclerAdapter";

    public static class MessageTextViewHolder extends RecyclerView.ViewHolder {
        public TextView leftToRight;
        public TextView rightToLeft;
        public TextView received;

        public MessageTextViewHolder(View rowView) {
            super(rowView);
            leftToRight = (TextView) rowView.findViewById(R.id.left_to_right);
            rightToLeft = (TextView) rowView.findViewById(R.id.right_to_left);
            received = (TextView) rowView.findViewById(R.id.right_to_left_received);
        }
    }

    final List<UserMessage> messages = new ArrayList<>();
    static int resourceId = R.layout.message_text;
    private String thisUserId;
    private Context context;

    public MessageRecyclerAdapter(String thisUserId, Context context) {
        this.thisUserId = thisUserId;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if (messages != null) {
            return messages.size();
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(final MessageTextViewHolder messageTextViewHolder, final int position) {
        final UserMessage message = messages.get(position);

        messageTextViewHolder.received.setVisibility(View.GONE);
        if (message.getFromId().equals(thisUserId)) {
            messageTextViewHolder.leftToRight.setVisibility(View.GONE);
            messageTextViewHolder.rightToLeft.setText(message.getText());
            messageTextViewHolder.rightToLeft.setVisibility(View.VISIBLE);
            if (message.isReceived()) {
                messageTextViewHolder.received.setVisibility(View.VISIBLE);
                messageTextViewHolder.received.setText("isReceived");
            }
            if (message.isSent()) {
                messageTextViewHolder.received.setVisibility(View.VISIBLE);
                messageTextViewHolder.received.setText("isSent");
            }
        } else {
            messageTextViewHolder.rightToLeft.setVisibility(View.GONE);
            messageTextViewHolder.leftToRight.setText(message.getText());
            messageTextViewHolder.leftToRight.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public MessageTextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
        return (new MessageTextViewHolder(v));
    }

    public void addItem(UserMessage message) {
        messages.add(message);
        notifyDataSetChanged();
    }

    public void removeItem(int i) {
        messages.remove(i);
        notifyDataSetChanged();
    }

    public UserMessage getItem(int i) {
        if (i >= 0 && i < messages.size())
            return messages.get(i);
        else
            return null;
    }

    public  UserMessage getItemByHash(String hash){
        for (UserMessage msg : this.messages) {
            if(msg.getHash().equals(hash)){
                return msg;
            }
        }
        return null;
    }

    public void updateUserMessage(UserMessageSent userMessageSent){
        for (UserMessage msg : this.messages) {
            if(msg.getHash().equals(userMessageSent.getHash())){
                msg.setSent(true);
            }
        }
        notifyDataSetChanged();
    }

    public  void updateUserMessage(UserMessageReceived userMessageReceived){
        for (UserMessage msg : this.messages) {
            if(msg.getHash().equals(userMessageReceived.getHash())){
                msg.setSent(true);
                msg.setReceived(true);
            }
        }
        notifyDataSetChanged();
    }

    public void addAllItems(List<UserMessage> messages) {
        this.messages.clear();
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }

}