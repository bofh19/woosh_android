package in.w4rlock.woosh.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.utils.ApplicationProperties;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by saikrishnak on 6/2/15.
 */
public class ChatListRecyclerAdapter extends RecyclerView.Adapter<ChatListRecyclerAdapter.ChatPersonViewHolder> {

    private static final String DEBUG_TAG = "ChatListRecyclerAdapter";
    public static class ChatPersonViewHolder extends RecyclerView.ViewHolder {
        public ImageView profileImage;
        public TextView name;
        public TextView status;
        public TextView email;
        public View parent;
        public ChatPersonViewHolder(View rowView) {
            super(rowView);
            parent = rowView.findViewById(R.id.chat_profile_parent);
            profileImage = (ImageView) rowView.findViewById(R.id.chat_profile_image);
            name = (TextView) rowView.findViewById(R.id.chat_profile_name);
            email = (TextView) rowView.findViewById(R.id.chat_profile_email);
            status = (TextView) rowView.findViewById(R.id.chat_profile_status);
        }
    }

    final List<UserFriend> friends = new ArrayList<>();
    static int resourceId = R.layout.chat_person;
    private RecyclerItemSelected callback;
    private Context context;
    private String thisUserId;
    public interface RecyclerItemSelected{
        void onItemSelected(UserFriend friend);
    }

    public ChatListRecyclerAdapter(Context context, ChatListRecyclerAdapter.RecyclerItemSelected callback){
        this.callback = callback;
        this.context = context;
        thisUserId = ApiKeyControllder.getId(context);
    }

    @Override
    public int getItemCount() {
        if (friends != null) {
            return friends.size();
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(final ChatPersonViewHolder chatPersonViewHolder, final int position) {
        // TODO remove this code from here. the db calling part
        final UserFriend friend = friends.get(position);
        int size = cupboard().withDatabase(SocketMessageDb.getReadDatabase(context)).query(UserMessage.class).withSelection("fromId = ? AND toId = ? AND viewed = ?",""+friend.id,thisUserId,""+0).list().size();
        String display_name = friend.display_name != null ? friend.display_name : "";
        if (size > 0)
            display_name = display_name+"("+size+")";
        Glide.with(context).load(ApplicationProperties.getBitmapFullUrl(friend.bitmapUrl)).centerCrop().into(chatPersonViewHolder.profileImage);
        chatPersonViewHolder.name.setText(display_name);
        chatPersonViewHolder.email.setText(friend.email);
        chatPersonViewHolder.status.setText(friend.status);
        chatPersonViewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemSelected(friend);
            }
        });
    }

    @Override
    public ChatPersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
        return (new ChatPersonViewHolder(v));
    }

    public void addItem(UserFriend friend){
        friends.add(friend);
        notifyDataSetChanged();
    }

    public void removeItem(int i){
        friends.remove(i);
        notifyDataSetChanged();
    }

    public UserFriend getItem(int i){
        if(i>=0 && i<friends.size())
            return friends.get(i);
        else
            return null;
    }

    public void addAllItems(List<UserFriend> friends){
        this.friends.clear();
        this.friends.addAll(friends);
        notifyDataSetChanged();
    }
}
