package in.w4rlock.woosh.controllers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.service.models.UserContact;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class ContactsListRecyclerAdapter extends RecyclerView.Adapter<ContactsListRecyclerAdapter.ChatPersonViewHolder> {

    private static final String DEBUG_TAG = "ChatListRecyclerAdapter";

    public static class ChatPersonViewHolder extends RecyclerView.ViewHolder {
        public ImageView profileImage;
        public TextView name;
        public TextView status;
        public TextView email;
        public View parent;

        public ChatPersonViewHolder(View rowView) {
            super(rowView);
            parent = rowView.findViewById(R.id.chat_profile_parent);
            profileImage = (ImageView) rowView.findViewById(R.id.chat_profile_image);
            name = (TextView) rowView.findViewById(R.id.chat_profile_name);
            email = (TextView) rowView.findViewById(R.id.chat_profile_email);
            status = (TextView) rowView.findViewById(R.id.chat_profile_status);
            status.setVisibility(View.GONE);
        }
    }

    final List<UserContact> contacts = new ArrayList<>();
    static int resourceId = R.layout.chat_person;
    private RecyclerItemSelected callback;

    public interface RecyclerItemSelected {
        void onItemSelected(UserContact contact);
    }

    public ContactsListRecyclerAdapter(Context context, ContactsListRecyclerAdapter.RecyclerItemSelected callback) {
        this.callback = callback;
    }

    @Override
    public int getItemCount() {
        if (contacts != null) {
            return contacts.size();
        }
        return 0;
    }

    @Override
    public void onBindViewHolder(final ChatPersonViewHolder chatPersonViewHolder, final int position) {
        final UserContact contact = contacts.get(position);
        chatPersonViewHolder.name.setText(contact.display_name);
        chatPersonViewHolder.email.setText(contact.email);
        chatPersonViewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onItemSelected(contact);
            }
        });
    }

    @Override
    public ChatPersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resourceId, parent, false);
        return (new ChatPersonViewHolder(v));
    }

    public void addItem(UserContact contact) {
        contacts.add(contact);
        notifyDataSetChanged();
    }

    public void removeItem(int i) {
        contacts.remove(i);
        notifyDataSetChanged();
    }

    public UserContact getItem(int i) {
        if (i >= 0 && i < contacts.size())
            return contacts.get(i);
        else
            return null;
    }

    public void addAllItems(List<UserContact> contacts) {
        this.contacts.clear();
        this.contacts.addAll(contacts);
        notifyDataSetChanged();
    }
}
