package in.w4rlock.woosh.controllers.api_controllers;

/**
 * Created by saikrishnak on 5/19/15.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import in.w4rlock.woosh.models.ApiKey;
import in.w4rlock.woosh.network.AsyncTaskInterface;
import in.w4rlock.woosh.network.GetApiKeyFromToken;
import in.w4rlock.woosh.utils.ApplicationProperties;


public class ApiKeyControllder {
    public static final String TAG = "ApiKeyControllder";

    public static interface Callback {
        public void onStart();

        public void onEnd();

        public void onFailed();
    }

    public static void clearApiKey(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(ApplicationProperties.pref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ApplicationProperties.apiKeyPref);
        editor.apply();
    }

    public static void saveApiKey(ApiKey apiKey, Context context) {
        SharedPreferences prefs = context.getSharedPreferences(ApplicationProperties.pref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ApplicationProperties.apiKeyPref, apiKey.getApi_key());
        editor.putString(ApplicationProperties.apiUserId, String.valueOf(apiKey.id));
        editor.apply();
    }

    public static String getApiKey(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(ApplicationProperties.pref, Context.MODE_PRIVATE);
        return prefs.getString(ApplicationProperties.apiKeyPref, null);
    }

    public static String getId(Context context){
        SharedPreferences prefs = context.getSharedPreferences(ApplicationProperties.pref, Context.MODE_PRIVATE);
        return prefs.getString(ApplicationProperties.apiUserId, null);
    }

    public static boolean hasKey(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(ApplicationProperties.pref, Context.MODE_PRIVATE);
        return prefs.contains(ApplicationProperties.apiKeyPref);
    }

    public static ApiKey getKeyFromResponse(String response) throws Exception {
        Gson gson = new Gson();
        ApiKey apiKey = gson.fromJson(response, ApiKey.class);
        if (apiKey.getApi_key().contains("failed"))
            return null;
        else
            return apiKey;
    }

    public static void fetchAndSaveApiKey(final String token, final ApiKeyControllder.Callback callback, final Context context) {
        AsyncTaskInterface<String> interface1 = new AsyncTaskInterface<String>() {

            @Override
            public void onResult(String t) {
                try {
                    Log.d(TAG, "Result: " + t);
                    ApiKey key = getKeyFromResponse(t);
                    if (key == null)
                        callback.onFailed();
                    else {
                        saveApiKey(key, context);
                    }
                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                    callback.onFailed();
                }
                callback.onEnd();
            }

            @Override
            public void onProgressUpdate(int percent) {
            }

            @Override
            public void onLoadingStarted() {
                callback.onStart();
            }

            @Override
            public void onLoadingEnded() {
            }

            @Override
            public void onError(Exception e) {
                Log.d(TAG, "onError: " + e.toString());
            }
        };
        GetApiKeyFromToken.AsyncTasker asyncTasker = new GetApiKeyFromToken.AsyncTasker(interface1);
        asyncTasker.execute(token);
    }
}
