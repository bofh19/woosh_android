package in.w4rlock.woosh.imagepick;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.utils.L;

public class AlbumPickerFragment extends Fragment {
    private static final String TAG = "AlbumPickerFragment";
    private static final String ARG_ALL_IMAGES = "all_images";

    public static AlbumPickerFragment newInstance(ArrayList<ImagesHolder> allImages) {
        AlbumPickerFragment fragment = new AlbumPickerFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_ALL_IMAGES, allImages);
        fragment.setArguments(args);
        return fragment;
    }

    public AlbumPickerFragment() {
    }

    ArrayList<ImagesHolder> allimgPaths;
    ArrayList<ImagesHolder> imgPaths;
    View rootView;
    private AlbumsAdapter imageAdapter;
    AlbumSelectedListener albumSelectedListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            allimgPaths = (ArrayList<ImagesHolder>) getArguments().getSerializable(ARG_ALL_IMAGES);
        } else {
            L.d(TAG, "no arguments killing activity");
            getActivity().finish();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_album_picker, container, false);
        ArrayList<ImagesHolder> uniqueImagesNoCount = new ArrayList<ImagesHolder>();
        uniqueImagesNoCount.addAll(ImagesHolder.getUniqueBucketNames(allimgPaths));
        imgPaths = new ArrayList<>();
        imgPaths.addAll(ImagesHolder.changeBucketNamesWithCount(uniqueImagesNoCount, allimgPaths));
        imageAdapter = new AlbumsAdapter(getActivity().getApplicationContext(), R.layout.row_photos_albums_item, imgPaths);
        ListView listView = (ListView) rootView.findViewById(R.id.albums_list_view_layout);
        listView.setAdapter(imageAdapter);
        L.d(TAG, "adapter set");

        listView.setOnItemClickListener(mMessageClickedHandler);
        L.d(TAG, "on item click handle set");
        return rootView;
    }

    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2, long arg3) {
            L.d(TAG, "adapter on item click called");
            albumSelectedListener.onAlbumSelected(imgPaths.get(arg2));
            System.gc();
        }
    };


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            albumSelectedListener = (AlbumSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface AlbumSelectedListener {
        void onAlbumSelected(ImagesHolder selectedImageAlbum);
    }


    public class AlbumsAdapter extends ArrayAdapter<ImagesHolder> {
        private Context mContext;
        ArrayList<ImagesHolder> mList;
        int layoutId;

        public AlbumsAdapter(Context c, int layoutId, ArrayList<ImagesHolder> imageList) {
            super(c, layoutId, imageList);
            this.mContext = c;
            this.mList = imageList;
            this.layoutId = layoutId;
        }

        class ViewHolder {
            ImageView imageview;
            TextView bucketName;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View row = convertView;
            if (row == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(layoutId, parent, false);
                holder.imageview = (ImageView) row.findViewById(R.id.imageView1);
                holder.bucketName = (TextView) row.findViewById(R.id.textView1);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            holder.bucketName.setText(mList.get(position).alteredBucketName);

            String location = "file://" + mList.get(position).path;
            Glide.with(mContext).load(mList.get(position).path).centerCrop().into(holder.imageview);
            return row;
        }
    }
}
