package in.w4rlock.woosh.imagepick;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.utils.BitmapUtils;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;

public class ImagePickerFragment extends Fragment {
    private static final String TAG = "ImagePickerFragment";
    ArrayList<ImagesHolder> imgPaths;
    View rootView;
    private int pickType;
    private ImageAdapter imageAdapter;
    int selectedItemsCount = 0;
    GridView gridView;
    private ImagePickerFragmentCallback callback;

    public static ImagePickerFragment newInstance(int photoPickType) {
        ImagePickerFragment fragment = new ImagePickerFragment();
        Bundle args = new Bundle();
        args.putInt(ImagePickerActivity.ARG_PHOTO_PICK_TYPE, photoPickType);
        fragment.setArguments(args);
        return fragment;
    }

    public ImagePickerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pickType = getArguments().getInt(ImagePickerActivity.ARG_PHOTO_PICK_TYPE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_image_picker_tab, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.image_picker_done:
                doDone();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void doDone() {
        Intent returnIntent = new Intent();
        ArrayList<String> selectedItems = new ArrayList<String>();
        for (ImagesHolder each : imgPaths) {
            if (each.isSelected)
                selectedItems.add(each.path);
        }
        returnIntent.putExtra("selectedItems", selectedItems);
        L.d(getActivity().getClass().getName(), "Selected Items: " + selectedItems.toString());
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        getActivity().finish();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.fragment_image_picker, container, false);

        imgPaths = new ArrayList<ImagesHolder>();
        gridView = (GridView) rootView
                .findViewById(R.id.photos_grid_view_layout_grid_view_item);
        InitilizeGridLayout();
        imageAdapter = new ImageAdapter(getActivity().getApplicationContext(),
                R.layout.row_multiphoto_item, imgPaths, columnWidth);
        gridView.setAdapter(imageAdapter);
        L.d(TAG, "adapter set");

        gridView.setOnItemClickListener(mMessageClickedHandler);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (ImagePickerFragmentCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public interface ImagePickerFragmentCallback {
        void onHomeBackPressed();
    }

    /*
    critical methods
     */

    public void onAlbumChanged(final ArrayList<ImagesHolder> result) {
        imgPaths.clear();
        imageAdapter.notifyDataSetChanged();
        GenerateThumbnailsTask generateThumbnailsTask = new GenerateThumbnailsTask(result);
        generateThumbnailsTask.execute();
    }

    public void onAlbumChangedAfterAsync(final ArrayList<ImagesHolder> result) {
        imgPaths.clear();
        imgPaths.addAll(result);
        imageAdapter.notifyDataSetChanged();
        if (gridView != null) {
            gridView.smoothScrollToPosition(0);
        }
    }

    private AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2, long arg3) {
            L.d(TAG, "adapter on item click called");
            if (pickType == ImagePickerActivity.SINGLE_PHOTO_PICK) {
                imgPaths.get(arg2).isSelected = true;
                doDone();
            } else {
                if (!imgPaths.get(arg2).isSelected) {
                    imgPaths.get(arg2).isSelected = true;
                    selectedItemsCount = selectedItemsCount + 1;
                } else {
                    imgPaths.get(arg2).isSelected = false;
                    selectedItemsCount = selectedItemsCount - 1;
                }
                imageAdapter.notifyDataSetChanged();
                L.d(TAG, "notify data set called");
            }
            System.gc();
        }
    };

    public class ImageAdapter extends ArrayAdapter<ImagesHolder> {
        private Context mContext;
        ArrayList<ImagesHolder> mList;
        int layoutId;
        int columnWidth;

        public ImageAdapter(Context c, int layoutId,
                            ArrayList<ImagesHolder> imageList, int columnWidth) {
            super(c, layoutId, imageList);
            this.mContext = c;
            this.mList = imageList;
            this.layoutId = layoutId;
            this.columnWidth = columnWidth;
        }

        class ViewHolder {
            ImageView imageview;
            ImageView isSelected;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View row = convertView;
            if (row == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(layoutId, parent, false);
                holder.imageview = (ImageView) row.findViewById(R.id.imageView1);
                holder.isSelected = (ImageView) row.findViewById(R.id.imageView2);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            holder.imageview.setAdjustViewBounds(true);
            holder.imageview.setMaxHeight(columnWidth);
            holder.imageview.setMinimumHeight(columnWidth);
            holder.imageview.setMaxWidth(columnWidth);
            holder.imageview.setMinimumWidth(columnWidth);

            if (mList.get(position).isSelected) {
                holder.isSelected.setVisibility(View.VISIBLE);
            } else {
                holder.isSelected.setVisibility(View.GONE);
            }
            String location = mList.get(position).thumbnailPath;
            L.d(TAG, "path is " + mList.get(position).path);

            Glide.with(mContext).load(location)
                    .asBitmap().into(new SimpleTarget<Bitmap>(columnWidth, columnWidth) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    holder.imageview.setImageBitmap(resource);
                }
            });

            L.d(TAG, "path is " + mList.get(position).path);
            return row;
        }

    }

    class GenerateThumbnailsTask extends AsyncTask<Void, Void, ArrayList<ImagesHolder>> {
        ArrayList<ImagesHolder> givenImages;

        public GenerateThumbnailsTask(ArrayList<ImagesHolder> givenImages) {
            this.givenImages = givenImages;
        }

        private ProgressDialog progressDialog;

        @Override
        public void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
        }

        @Override
        protected ArrayList<ImagesHolder> doInBackground(Void... params) {
            File f = new File(getActivity().getCacheDir().getAbsolutePath() + "/thumbnails11");

            if (!f.exists()) {
                f.mkdirs();
            }
            FileOutputStream out = null;
            for (ImagesHolder imagesHolder : givenImages) {
                File x = new File(f.getAbsolutePath() + imagesHolder.path.hashCode());
                imagesHolder.thumbnailPath = imagesHolder.path;
                if (!x.exists()) {
                    try {
                        out = new FileOutputStream(x);
                        L.d(TAG, "DECODING PATH " + imagesHolder.path);
                        Bitmap b = BitmapUtils.decodeFile(imagesHolder.path + "", columnWidth, columnWidth);
                        b.compress(Bitmap.CompressFormat.JPEG, 50, out);
                        b.recycle();
                        L.d(TAG, "THUMBNAIL Sucess : " + x.getAbsolutePath());
                        imagesHolder.thumbnailPath = x.getAbsolutePath();
                    } catch (Exception e) {
                        L.d(TAG, "THUMBNAIL Failed 1: " + e.toString());
                    } finally {
                        try {
                            out.close();
                        } catch (Exception ignore) {
                            L.d(TAG, "THUMBNAIL Failed 2 " + ignore.toString());
                        }
                    }
                } else {
                    imagesHolder.thumbnailPath = x.getAbsolutePath();
                    L.d(TAG, "THUMBNAIL thumb already exists no need again");
                }
            }
            return givenImages;
        }

        public void onPostExecute(ArrayList<ImagesHolder> result) {
            progressDialog.dismiss();
            onAlbumChangedAfterAsync(result);
        }

    }


    public int columnWidth;

    private void InitilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, GRID_PADDING, r.getDisplayMetrics());

        columnWidth = (int) ((Utils.getScreenWidth(getActivity().getApplicationContext()) - ((NUM_OF_COLUMNS + 1) * padding)) / NUM_OF_COLUMNS);

        gridView.setNumColumns(NUM_OF_COLUMNS);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }

    public static final int NUM_OF_COLUMNS = 3;
    public static final int GRID_PADDING = 8; // in dp
}
