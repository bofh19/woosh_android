package in.w4rlock.woosh.imagepick;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.R;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;


public class ImagePickerActivity extends AppCompatActivity implements AlbumPickerFragment.AlbumSelectedListener, ImagePickerFragment.ImagePickerFragmentCallback {
    private static final String TAG = "ImagePickerActivity";
    public static final String ARG_PHOTO_PICK_TYPE = "arg_photo_pick_type";
    public static final int SINGLE_PHOTO_PICK = 1;
    public static final int MULTI_PHOTO_PICK = 2;
    private ViewPager mViewPager;

    @Override
    public void onBackPressed() {
        if(mViewPager != null && mViewPager.getCurrentItem() == 1){
            mViewPager.setCurrentItem(0);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_picker);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        if (mViewPager != null) {
            setUpImages();
        }
    }

    ArrayList<ImagesHolder> allImages;
    ImagePickerFragment imagePickerFragment;

    private void setUpImages() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //if (!Settings.System.canWrite(this) ) {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
            } else {
                (new LoadAlbumImages()).execute();
            }
        } else {
            (new LoadAlbumImages()).execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    L.d(TAG, "Permission Granted");
                    (new LoadAlbumImages()).execute();
                } else {
                    L.d(TAG, "Permission Denied");
                    this.finish();
                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        imagePickerFragment = ImagePickerFragment.newInstance(SINGLE_PHOTO_PICK);
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(AlbumPickerFragment.newInstance(allImages), "Pick Album");
        adapter.addFragment(imagePickerFragment, "Pick Image");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if(mViewPager != null && mViewPager.getCurrentItem() == 1){
                mViewPager.setCurrentItem(0);
            }else {
                finish();
            }
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAlbumSelected(final ImagesHolder selectedImageAlbum) {
        L.d(TAG, "" + selectedImageAlbum);
        if (imagePickerFragment != null) {

            class LoadImagesOfAlbum extends AsyncTask<Void, Void, ArrayList<ImagesHolder>> {
                private ProgressDialog progressDialog;

                @Override
                public void onPreExecute() {
                    progressDialog = new ProgressDialog(ImagePickerActivity.this);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage(getString(R.string.loading));
                    progressDialog.show();
                }

                @Override
                protected ArrayList<ImagesHolder> doInBackground(Void... params) {
                    ArrayList<ImagesHolder> result = ImagesHolder.getOnlyImagesOfThisAlbum(selectedImageAlbum, allImages);
                    FileOutputStream out = null;
                    File f = new File(getCacheDir().getAbsolutePath() + "/thumbnails/new");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    L.d(TAG, "THUMBNAIL cache path " + f.getAbsolutePath());
                    for (ImagesHolder imagesHolder : result) {
                        imagesHolder.isSelected = false;
                    }
                    return result;
                }


                public void onPostExecute(ArrayList<ImagesHolder> result) {
                    progressDialog.dismiss();
                    for (ImagesHolder img : result) {
                        img.isSelected = false;
                    }

                    imagePickerFragment.onAlbumChanged(result);
                    mViewPager.setCurrentItem(1);
                }
            }
            LoadImagesOfAlbum loadImagesOfAlbum = new LoadImagesOfAlbum();
            loadImagesOfAlbum.execute();
        }
    }

    @Override
    public void onHomeBackPressed() {
        mViewPager.setCurrentItem(0);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    class LoadAlbumImages extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected Void doInBackground(Void... params) {
            allImages = new ArrayList<>();
            try {
                ContentResolver cr = getContentResolver();
                String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.MINI_THUMB_MAGIC, MediaStore.Images.Media.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME, MediaStore.Images.Media.DATE_ADDED};
                Cursor cursor = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.DATE_ADDED + " DESC");
                if (cursor.moveToFirst()) {
                    int bucketColumn = cursor.getColumnIndex(
                            MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    int miniThumbColumn = cursor.getColumnIndex(MediaStore.Images.Media.MINI_THUMB_MAGIC);
                    int idColumn = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

                    do {
                        ImagesHolder newPath = new ImagesHolder();
                        newPath.path = cursor.getString(dataColumn);
                        newPath.bucketName = cursor.getString(bucketColumn);
                        newPath.id = cursor.getLong(idColumn);
                        newPath.miniThumbId = cursor.getLong(miniThumbColumn);
                        allImages.add(newPath);
                    } while (cursor.moveToNext());
                }
                cursor.close();

                cursor = cr.query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, projection, null, null, MediaStore.Images.Media.DATE_ADDED + " DESC");
                if (cursor.moveToFirst()) {
                    int bucketColumn = cursor.getColumnIndex(
                            MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                    int miniThumbColumn = cursor.getColumnIndex(MediaStore.Images.Media.MINI_THUMB_MAGIC);
                    int idColumn = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

                    do {
                        ImagesHolder newPath = new ImagesHolder();
                        newPath.path = cursor.getString(dataColumn);
                        newPath.bucketName = cursor.getString(bucketColumn);
                        newPath.id = cursor.getLong(idColumn);
                        newPath.miniThumbId = cursor.getLong(miniThumbColumn);
                        allImages.add(newPath);
                    } while (cursor.moveToNext());
                }

                cursor.close();

            } catch (Exception e) {
                L.d(TAG, "ERROR while loading photos from content resolver " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(ImagePickerActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(allImages.size() <= 0){
                        Utils.showShortToast(getApplicationContext(), " No photos in gallery ");
                    }
                    setupViewPager(mViewPager);
                }
            });
        }
    }

}
