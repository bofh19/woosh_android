package in.w4rlock.woosh.imagepick;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;


public class ImagesHolder implements Serializable {
	private static final String DEBUG_TAG = "ImagesHolder";
	public String path;
	public String bucketName;
	public String alteredBucketName = "";
	public boolean isSelected = false;
	public long id;
	public long miniThumbId;
	public String thumbnailPath;
	@Override
	public boolean equals(Object obj) {
		ImagesHolder other=(ImagesHolder) obj;
	   return this.bucketName.equals(other.bucketName);
	}
	
	@Override
	public int hashCode() {
	    return bucketName.hashCode();
	}
	
	public static Set<ImagesHolder> getUniqueBucketNames(ArrayList<ImagesHolder> imagesSelectionHolders){
		LinkedHashSet<ImagesHolder> setImagesHolder = new LinkedHashSet<ImagesHolder>();
		setImagesHolder.addAll(imagesSelectionHolders);
		return setImagesHolder;
	}
	
	public static ArrayList<ImagesHolder> changeBucketNamesWithCount(ArrayList<ImagesHolder> uniqueImagesList, ArrayList<ImagesHolder> allImagesList){
		ArrayList<ImagesHolder> imagesListWithCount = new ArrayList<ImagesHolder>();
		for(ImagesHolder i:uniqueImagesList){
			ImagesHolder thisImage = new ImagesHolder();
			thisImage.path = i.path;
			thisImage.bucketName = i.bucketName;
			thisImage.alteredBucketName = i.bucketName+" ("+getThisBucketCount(i, allImagesList)+")";
			imagesListWithCount.add(thisImage);
		}
		return imagesListWithCount;
	}
	
	public static Integer getThisBucketCount(ImagesHolder img1,ArrayList<ImagesHolder> allImagesList){
		Integer count = 0;
		for(ImagesHolder j:allImagesList){
//			Log.d(DEBUG_TAG, "IMAGES_CHECK_AT_" + j.bucketName + " / " + img1.bucketName);
			if(img1.bucketName.equals(j.bucketName)){
				count = count + 1;
//				Log.d(DEBUG_TAG, "IMAGES_EQUAL_"+j.bucketName+ " / "+img1.bucketName);
			}else{
//				Log.d(DEBUG_TAG, "IMAGES_NOT_EQUAL_"+j.bucketName+ " / "+img1.bucketName);
			}
		}
		return count;
	}
	
	public static ArrayList<ImagesHolder> getOnlyImagesOfThisAlbum(ImagesHolder selectedImageAlbum, ArrayList<ImagesHolder> allImagesList){
		ArrayList<ImagesHolder> imagesListWithThisAlbum = new ArrayList<ImagesHolder>();
		for(ImagesHolder i : allImagesList){
			if(i.bucketName.equals(selectedImageAlbum.bucketName)){
				imagesListWithThisAlbum.add(i);
			}
		}
		System.gc();
		return imagesListWithThisAlbum;
	}
}