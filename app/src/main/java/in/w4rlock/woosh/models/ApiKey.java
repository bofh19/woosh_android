package in.w4rlock.woosh.models;

/**
 * Created by saikrishnak on 5/19/15.
 */
public class ApiKey {
    private String api_key;
    public int id;

    public String getApi_key() {
        return api_key;
    }
    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }
    @Override
    public String toString() {
        if(api_key == null)
            return null;
        else return api_key;
    }

}

