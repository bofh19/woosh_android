//package in.w4rlock.woosh;
//
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//
//
//public class ChatList extends Activity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_chat_list);
//
//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.frame_container, new LoginFragment())
//                    .commit();
//        }
//
//        findViewById(R.id.frag_1).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.frame_container, new LoginFragment())
//                        .commit();
//            }
//        });
//
//        findViewById(R.id.frag_2).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.frame_container, new ChatListFragment())
//                        .commit();
//            }
//        });
//
//        findViewById(R.id.frag_3).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getFragmentManager().beginTransaction()
//                        .replace(R.id.frame_container, new ChatFragment())
//                        .commit();
//            }
//        });
//
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_chat_list, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    public static final int CHAT_FRAGMENT = 3;
//    public void changeFragmentTo(int i){
//        if(i == CHAT_FRAGMENT){
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.frame_container, new ChatFragment())
//                    .commit();
//        }
//    }
//
//}
