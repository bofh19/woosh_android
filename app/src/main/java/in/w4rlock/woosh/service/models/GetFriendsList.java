package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

public class GetFriendsList extends SocketMessage {

    public GetFriendsList() {
        operation = ServerOperationCodes.friends_list;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<GetFriendsList>() {
        }.getType();
        return gson.toJson(this, type);
    }
}