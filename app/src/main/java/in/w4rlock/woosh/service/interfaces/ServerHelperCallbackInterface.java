package in.w4rlock.woosh.service.interfaces;

import java.util.ArrayList;

import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 6/12/15.
 */
public interface ServerHelperCallbackInterface {
    void onSendMessageFailed();
    void dbChanged(DBChanged changeId); // when local db changes

    // when server db changes, but not local db
    // means probably either change is incoming
    // or request server for those changes
    void onServerUpdate(ServerUpdate serverUpdate);
    void aDummyMethod();

    void onUserMessage(UserMessage userMessage);

    void onUserMessageSent(UserMessageSent userMessageSent);

    void onUserMessageReceived(UserMessageReceived userMessageReceived);

    void onUserProfile(UserProfile userProfile);

    void onUserFriendsList(ArrayList<UserFriend> friends);

    void onUserContactsList(ArrayList<UserContact> contacts);

    void onSendUserMessage(UserMessage userMessage);

    void onAddUserFriendResponse(AddUserFriend addUserFriend);
}
