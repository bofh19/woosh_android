package in.w4rlock.woosh.service;

/**
 * Created by saikrishnak on 5/21/15.
 */
public class ServerOperationCodes {
    public static final String operation = "operation";
    public static final String user_message = "user_message";
    public static final String to_user = "to_user";
    public static final String hash_key = "hash_key";
    public static final String ping = "ping";
    public static final String pong = "pong";
    public static final String friends_list = "friends_list";
    public static final String friend = "friend";
    public static final String user_message_received = "user_message_received";
    public static final String user_message_sent = "user_message_sent";
    public static final String contacts_list = "contacts_list";
    public static final String contact = "contact";
    public static final String profile_pic = "profile_pic";
    public static final String filename = "filename";
    public static final String profile = "profile";
    public static final String os_type = "os_type";
    public static final String os_android = "os_android";
    public static final String os_version = "os_version";
    public static final String user_status = "user_status";
    public static final String user_display_name = "user_display_name";

    public static final String add_friend = "add_friend";
    public static final String add_friend_response = "add_friend_response";
    public static final String add_friend_from_token = "add_friend_from_token";
    public static final String update_friend = "update_friend";
}
