package in.w4rlock.woosh.service;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerSocketInterface;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;
import in.w4rlock.woosh.utils.ApplicationProperties;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;

/**
 * w4rlock scripts
 * Created by saikrishnak on 2/24/15.
 */
public class Server {
    public static Socket socket;
    public ServerSocketInterface callback;
    private static final String TAG = "SocketServer";
    private static boolean listenRunning = false;
    private static boolean writeRunning = false;
    public static String apiKey = "";
    private static final String BREAK_SOCKET = "break_socket_message";
    public Server(){

    }

    DecodeServerMessage.DecodeServerMessageCallback decodeServerMessageCallback =  new DecodeServerMessage.DecodeServerMessageCallback() {
        @Override
        public void onPing() {
            Log.d(TAG,"onPing");
        }

        @Override
        public void onPong() {
            Log.d(TAG,"onPong");
        }

        @Override
        public void onUserMessage(UserMessage userMessage) {
            Log.d(TAG,"onUserMessage "+userMessage);
            if(callback !=null)
                callback.onUserMessage(userMessage);
        }

        @Override
        public void onUnknownOperation(String message) {
            Log.d(TAG,"onUnknownOperation "+message);
        }

        @Override
        public void onUserFriendsList(ArrayList<UserFriend> friends) {
            Log.d(TAG,"onUserFriendsList: "+friends);
            callback.onUserFriendsList(friends);
        }

        @Override
        public void onUserContactsList(ArrayList<UserContact> contacts) {
            Log.d(TAG,"onUserContactsList: "+contacts);
            callback.onUserContactsList(contacts);
        }

        @Override
        public void onUserMessageSent(UserMessageSent userMessageSent) {
            if(callback != null)
                callback.onUserMessageSent(userMessageSent);
        }

        @Override
        public void onUserMessageReceived(UserMessageReceived userMessageReceived) {
            if(callback != null)
                callback.onUserMessageReceived(userMessageReceived);
        }

        @Override
        public void onServerUpdate(ServerUpdate serverUpdate){
            if(callback != null)
                callback.onServerUpdate(serverUpdate);
        }

        @Override
        public void onUserProfile(UserProfile userProfile) {
            if(callback != null)
                callback.onUserProfile(userProfile);
        }

        @Override
        public void onAddUserFriend(UserFriend userFriend) {
            if(callback != null)
                callback.onAddUserFriend(userFriend);
        }

        @Override
        public void onAddUserFriendResponse(AddUserFriend addUserFriend) {
            if(callback != null)
                callback.onAddUserFriendResponse(addUserFriend);
        }

        @Override
        public void onUpdateFriend(UserFriend updateUserFriend) {
            if(callback != null)
                callback.onUpdateUserFriend(updateUserFriend);
        }
    };

    /* retrying on failure */
    private static int retryCounter = 0;
    private static boolean retryOnSleep = false;
    private static final int TIME_OUT_TO_RECONNECT = 2000; // 2 sec
    private static final int MAX_RETRIES = 10;
    private static int totalRetriesLeft = 0;
    private Context ctx;
    public void retryConnection(Context context){
        return; // not going to anything for now
//        this.ctx = context;
//        if(retryOnSleep){
//            totalRetriesLeft = totalRetriesLeft + 1;
//        }else{
//            if(retryCounter >= MAX_RETRIES){
//                totalRetriesLeft = 0;
//            }else{
//                mHandler.postDelayed(mReconnectServerAfterTimeout, TIME_OUT_TO_RECONNECT);
//                retryOnSleep = true;
//            }
//        }
    }


    private final Handler mHandler = new Handler();
    private final Runnable mReconnectServerAfterTimeout = new Runnable() {
        @Override
        public void run() {
            retryOnSleep = false;
            L.d(TAG,"mReconnectServerAfterTimeout");
            if(Utils.isOnline(ctx) && retryCounter < 10) {
                retryCounter = retryCounter + 1;
                if(!isServerRunning()){
                    forceCloseSocket();
                    startServerIfNotRunning();
                    L.d(TAG, "called mserver");
                }

            }else{
                L.d(TAG, "phone is not even online we don't care about retries just make it max and leave it to the os broadcast receiver");
                retryCounter = MAX_RETRIES;
                totalRetriesLeft = 0;
            }

        }
    };

    private void socketConnected(){
        callback.onSocketConnected();
    }

    private void socketConnectionFailed(){
        forceCloseSocket();
        callback.onSocketConnectionFailed();
    }

    private void sendMessageFailed(String message){
        callback.onSendMessageFailed(message);
    }

    public void addCallback(ServerSocketInterface callback){
        this.callback = callback;
        startServer();
    }

    public boolean isServerRunning(){
        if(!listenRunning && writeRunning){
            forceCloseSocket();
            return false;
        }
        if (listenRunning && !writeRunning){
            forceCloseSocket();
            return false;
        }
        return listenRunning && writeRunning;
    }

    public void startServerIfNotRunning(){
        Log.d(TAG,"listenRunning: "+listenRunning+" writeRunning: "+writeRunning);
        if(!isServerRunning())
            startServer();
    }

    public void forceCloseSocket() {
        try {
            socket.close();
        } catch (Exception e) {
            Log.d(TAG,"unable to close socket on forceClose "+e.toString());
            e.printStackTrace();
        }
        listenRunning = false;
        if(writeRunning)
            try {
                writeQueue.put(BREAK_SOCKET);
                while(writeRunning){
                    // to avoid race condition we wait
                }
            } catch (InterruptedException e) {
                Log.d(TAG,"unable to add BREAK_SOCKET to write queue Exception "+e.toString());
            }
    }

    public void startServer() {
        Log.d(TAG,"listenRunning: "+listenRunning+" writeRunning: "+writeRunning);
        if(!listenRunning)
            (new Thread(listenRunnable)).start();
        if(!writeRunning)
            (new Thread(writeRunnable)).start();
    }

    public void startWrite(){
        if(!writeRunning)
            (new Thread(writeRunnable)).start();
    }

    public  void flushWrites(){
        writeQueue.clear();
    }

    public void writeToSocket(String final_message) {
        try {
            writeQueue.put(final_message);
            if(!isServerRunning()){
                startServerIfNotRunning();
            }
        } catch (InterruptedException e) {
            Log.d(TAG, "unable to add message from writeToSocket to write queue Exception " + e.toString());
            sendMessageFailed(final_message);
        }
    }


    final WriteAsyncCallback writeAsyncCallback =  new WriteAsyncCallback() {
        @Override
        public void writePreExecute() {

        }

        @Override
        public void writePostExecute(boolean written, String socketMessage) {
            if(!written) {
                sendMessageFailed(socketMessage);
            }
        }
    };

    public interface WriteAsyncCallback{
        void writePreExecute();
        void writePostExecute(boolean written,String socketMessage);
    }

    public Runnable writeRunnable = new Runnable() {
        @Override
        public void run() {
            writeOnSocket(writeAsyncCallback);
        }
    };

    private final BlockingQueue writeQueue = new PriorityBlockingQueue();

    private void writeOnSocket(final WriteAsyncCallback callback){
        writeRunning = true;
        try {
            while (socket!= null && socket.isConnected()) {
                Log.d(TAG,"blocking");
                String message = (String)writeQueue.take();
                if(message == null || message.length() <= 0){
                    continue;
                }
//                Log.d(TAG, "blocking ended Write doInBackground GOT new Message: "+message);
                if (message.equals(BREAK_SOCKET)){
                    Log.d(TAG,"break loop");
                    break;
                }
                callback.writePreExecute();
                OutputStream ouStream;
                try {
                    if (socket == null || socket.isClosed()) {
                        callback.writePostExecute(false, message);
                        break;
                    }
                    ouStream = socket.getOutputStream();
                    ouStream.write(message.getBytes());
                    ouStream.flush();
                    callback.writePostExecute(true,message);
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.writePostExecute(false,message);
                }
            }
        } catch (InterruptedException ex) {
            Log.d(TAG,"Exception WriteOnSocket: "+ex.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        writeRunning = false;
    }

    public Runnable listenRunnable = new Runnable() {
        @Override
        public void run() {
            listenOnSocket();
        }
    };

    public void listenOnSocket() {
        if(listenRunning)
            return;
        Log.d(TAG,"listenOnSocket");
        if (socket == null || socket.isClosed()) {
            try {
                socket = new Socket(ApplicationProperties.SERVER, ApplicationProperties.PORT);
                listenRunning = true;
                startWrite();
                socketConnected();
            } catch (Exception e) {
                e.printStackTrace();
                socketConnectionFailed();
                forceCloseSocket();
                return;
            }
        }
        InputStream inStream;
        Log.d(TAG,"listenOnSocket: "+socket.isConnected());
        while (socket.isConnected()) {
            Log.d(TAG,"trying to read from socket");
            listenRunning = true;
            try {
                inStream = socket.getInputStream();
                byte[] byteBuffer = new byte[26];
                int numBytes = inStream.read(byteBuffer);
                byte[] tempBuffer = new byte[numBytes];
                System.arraycopy(byteBuffer, 0, tempBuffer, 0, numBytes);
                String message = new String(tempBuffer, "UTF-8");
                String key = message.substring(0,16);
                int lengthOfData = Integer.parseInt(message.substring(16,26));
                Log.d(TAG, "message: " + message + " key: " + key + " lengthOfData: " + lengthOfData);
                if(key.equals(Server.apiKey)){
                    String encodedFinalMessage = "";
                    int bytesRemaining = lengthOfData;
                    while(bytesRemaining > 0){
//                        Log.d(TAG,"bytesRemaining: "+bytesRemaining);
                        if(bytesRemaining < 1024){
//                            Log.d(TAG, "bytesRemaining: in if");
                            byteBuffer = new byte[bytesRemaining];
                            bytesRemaining = 0;
                        }else{
//                            Log.d(TAG, "bytesRemaining: in else");
                            bytesRemaining = bytesRemaining - 1024;
                            byteBuffer = new byte[1024];
                        }
//                        Log.d(TAG,"bytesRemaining: "+bytesRemaining);
                        numBytes = inStream.read(byteBuffer);
                        tempBuffer = new byte[numBytes];
                        System.arraycopy(byteBuffer, 0, tempBuffer, 0, numBytes);
                        message = new String(tempBuffer, "UTF-8");
                        encodedFinalMessage += message;
                    }
                    Log.d(TAG,"encodedFinalMessageLength: "+encodedFinalMessage.length()+" actualLength: "+lengthOfData );
                    while(encodedFinalMessage.length() < lengthOfData){
                        int left_out = lengthOfData - encodedFinalMessage.length();
                        byteBuffer = new byte[left_out];
                        numBytes = inStream.read(byteBuffer);
                        tempBuffer = new byte[numBytes];
                        System.arraycopy(byteBuffer, 0, tempBuffer, 0, numBytes);
                        message = new String(tempBuffer, "UTF-8");
                        encodedFinalMessage += message;
                    }
                    Log.d(TAG,"encodedFinalMessageLength: "+encodedFinalMessage.length()+" actualLength: "+lengthOfData );
                    try {
                        DecodeServerMessage.decodeServerMessage(encodedFinalMessage, decodeServerMessageCallback);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(TAG,"failed to parse server message");
                    }
                }else{
                    Log.d(TAG,"Key And ServerKey are not equal Key: "+key+" "+Server.apiKey);
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }catch (NegativeArraySizeException e){
                e.printStackTrace();
                break;
            }catch (NumberFormatException e){
                e.printStackTrace();
                break;
            }catch (StringIndexOutOfBoundsException e){
                e.printStackTrace();
                break;
            }catch (Exception e){
                e.printStackTrace();
                break;
            }

        }

        listenRunning = false;
        socketConnectionFailed();
    }
}