package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public class UserContact extends SocketMessage {
    public Long _id;
    public int id;
    public String email;
    public String display_name;
    public boolean claimed;
    public boolean invited;

    public void UserContactextends() {
        operation = ServerOperationCodes.contact;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserContact>() {
        }.getType();
        return gson.toJson(this, type);
    }

    public static UserContact getFromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserContact>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}