package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/9/15.
 */
public class UserProfile  extends SocketMessage {
    public Long _id;
    public String display_name;
    public int id;
    public String email;
    public String bitmapUrl;
    public String user_status;

    public void UserProfile() {
        operation = ServerOperationCodes.profile;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserProfile>() {
        }.getType();
        return gson.toJson(this, type);
    }

    public static UserProfile getFromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserProfile>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}