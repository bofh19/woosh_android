package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * Created by skarumuru on 4/25/2016.
 */
public class AddUserFriend extends SocketMessage {

    private String authKey;
    private String userUniqueDetail;
    private String addUserError;

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<AddUserFriend>(){}.getType();
        return gson.toJson(this, type);
    }

    public static AddUserFriend getFromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<AddUserFriend>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public  String toString(){
        return this.toJson();
    }

    public AddUserFriend(){
        super();
        this.operation =  ServerOperationCodes.add_friend;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getUserUniqueDetail() {
        return userUniqueDetail;
    }

    public void setUserUniqueDetail(String userUniqueDetail) {
        this.userUniqueDetail = userUniqueDetail;
    }

    public String getAddUserError() {
        return addUserError;
    }

    public void setAddUserError(String addUserError) {
        this.addUserError = addUserError;
    }
}
