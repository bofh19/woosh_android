package in.w4rlock.woosh.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerSocketInterface;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.PingOperation;
import in.w4rlock.woosh.service.models.SocketMessage;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;

/**
 * w4rlock scripts
 * Created by saikrihnak on 2/24/15.
 */
public class ServerService extends Service implements Serializable{


    private static final String TAG = "ServerService";
    private IBinder mBinder;
    private NotificationManager mNotificationManager;
    public static final String PACKAGE_NAME = "in.w4rlock.woosh";
    public static final String START_SERVICE = "in.w4rlock.woosh.start_service";
    public static final String ACTION_RECONNECT_SOCKET = "in.w4rlock.woosh.service.ACTION_RECONNECT_SOCKET";

    private static final Server mServer = new Server();
    private static ServerHelper mServerHelper;


    public ServerSocketInterface mServerCallback = new ServerSocketInterface() {
        @Override
        public void onSocketConnected() {
            if(mServerHelper != null)
                mServerHelper.onSocketConnected();
            L.d(TAG, "onSocketConnected");
            PingOperation socketMessage = new PingOperation();
            writeMessage(socketMessage);

        }

        @Override
        public void onSocketConnectionFailed() {
            mServerHelper.onSocketConnectionFailed();
            L.d(TAG,"onSocketConnectionFailed: ");
            mServer.retryConnection(getApplicationContext());
        }


        @Override
        public void onSendMessageFailed(String socketMessage) {
            mServerHelper.onSendMessageFailed(socketMessage);
            L.d(TAG, "onSendMessageFailed: " + socketMessage);
            mServer.retryConnection(getApplicationContext());
            writeMessage(socketMessage);
        }

        @Override
        public void onUserMessage(UserMessage userMessage) {
            mServerHelper.onUserMessage(userMessage);
        }

        @Override
        public void onUserMessageSent(UserMessageSent userMessageSent) {
            mServerHelper.onUserMessageSent(userMessageSent);
        }

        @Override
        public void onUserMessageReceived(UserMessageReceived userMessageReceived) {
            mServerHelper.onUserMessageReceived(userMessageReceived);
        }

        @Override
        public void onUserProfile(UserProfile userProfile) {
            mServerHelper.onUserProfile(userProfile);
        }

        @Override
        public void onServerUpdate(ServerUpdate serverUpdate){
            mServerHelper.onServerUpdate(serverUpdate);
        }

        @Override
        public void onAddUserFriend(UserFriend userFriend) {
            mServerHelper.onAddUserFriend(userFriend);
        }

        @Override
        public void onAddUserFriendResponse(AddUserFriend addUserFriend) {
            mServerHelper.onAddUserFriendResponse(addUserFriend);
        }

        @Override
        public void onUpdateUserFriend(UserFriend updateUserFriend) {
            mServerHelper.onUpdateUserFriend(updateUserFriend);
        }

        @Override
        public void onUserFriendsList(ArrayList<UserFriend> friends) {
            mServerHelper.onUserFriendsList(friends);
        }

        @Override
        public void onUserContactsList(ArrayList<UserContact> contacts) {
            mServerHelper.onUserContactsList(contacts);
        }

    };

    @Override
    public void onCreate() {
        L.d(TAG, "debug: Creating in.w4rlock.woosh.service");
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Server.apiKey = ApiKeyControllder.getApiKey(getApplicationContext());
        mServer.addCallback(mServerCallback);
        mServerHelper = new ServerHelper(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        L.d(TAG, "server service on start called");
        if (intent == null || intent.getAction() == null) {
            L.d(TAG, "got null getAction");
            return START_NOT_STICKY;
        }
        String action = intent.getAction();
        L.d(TAG, "Got action: " + action);
        if (action.equals(ACTION_RECONNECT_SOCKET) && mServer != null){
            mServer.forceCloseSocket();
            mServer.startServerIfNotRunning();
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        mBinder = new ServerServiceBinder(this);
        return mBinder;
    }

    @Override
    public void onDestroy() {

    }

    public void writeMessage(List<SocketMessage> socketMessages){
        for(SocketMessage socketMessage:socketMessages) {
            L.d(TAG, "writing: " + socketMessage);
            try {
                mServer.writeToSocket(SocketMessage.prepareFinalSocketMessage(Server.apiKey, socketMessage));
            } catch (JSONException e) {
                e.printStackTrace();
                L.d(TAG, "failed to write to socket. error creating server json object");
            }
        }
    }

    private void writeMessage(String socketMessage){
        mServer.writeToSocket(socketMessage);
    }

    public void writeMessage(SocketMessage socketMessage){
        ArrayList<SocketMessage> socketMessages = new ArrayList<>();
        socketMessages.add(socketMessage);
        writeMessage(socketMessages);
    }

    public void executeLogout(){
        mServer.flushWrites();
        mServer.forceCloseSocket();
    }

    public ServerHelper getServerHelper(){
        return this.mServerHelper;
    }
}
