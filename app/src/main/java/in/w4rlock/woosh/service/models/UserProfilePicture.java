package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/8/15.
 */
public class UserProfilePicture extends SocketMessage{
    private String profile_pic;
    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public UserProfilePicture(){
        this.operation = ServerOperationCodes.profile_pic;

    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserProfilePicture>() {}.getType();
        return gson.toJson(this, type);
    }

    public static UserProfilePicture getFromJson(String json){
        Gson gson = new Gson();
        Type type = new TypeToken<UserProfilePicture>() {}.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public String toString(){
        String jsonStr = this.toJson();
        if(jsonStr.length() <= 100){
            return jsonStr;
        }else{
            String out = "";
            for(int i=0;i<50;i++){
                out = out + jsonStr.charAt(i);
            }
            out = out + ".................";
            for(int i=jsonStr.length()-50;i<jsonStr.length();i++){
                out = out + jsonStr.charAt(i);
            }
            return out;
        }
    }
}

