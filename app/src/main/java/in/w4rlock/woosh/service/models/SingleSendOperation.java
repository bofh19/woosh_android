package in.w4rlock.woosh.service.models;

import org.json.JSONException;
import org.json.JSONObject;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * Created by saikrishnak on 6/12/15.
 * a single operation code with a single piece of data to server.
 * output would be this -> { operation: given_operation, given_operation: given_data }
 */
public class SingleSendOperation extends SocketMessage {

    private String data;
    public SingleSendOperation(){

    }

    public SingleSendOperation(String operationCode, String data){
        this.operation = operationCode;
        this.data = data;
    }

    @Override
    public String toJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ServerOperationCodes.operation, this.operation);
            jsonObject.put(this.operation, this.data);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return jsonObject.toString();
    }
}
