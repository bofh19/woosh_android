package in.w4rlock.woosh.service;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.DbService;
import in.w4rlock.woosh.database.db.DbServiceInt;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerHelperInterface;
import in.w4rlock.woosh.service.interfaces.ServerSocketInterface;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.AddUserFriendFromToken;
import in.w4rlock.woosh.service.models.GetContactsList;
import in.w4rlock.woosh.service.models.GetFriendsList;
import in.w4rlock.woosh.service.models.GetUserProfile;
import in.w4rlock.woosh.service.models.SingleSendOperation;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;
import in.w4rlock.woosh.service.models.UserProfilePicture;
import in.w4rlock.woosh.utils.BitmapUtils;
import in.w4rlock.woosh.utils.Utils;



/**
 * Created by saikrishnak on 5/27/15.
 */
public class ServerHelper implements ServerHelperInterface,ServerSocketInterface {
    private static final String TAG = "ServerHelper";
    private ServerService mServerService;
    private static final ServerHelperObservers mServerHelperObservers = new ServerHelperObservers();
    DbServiceInt dbService ;
    ServerHelper(ServerService service){
        this.mServerService = service;
        this.dbService = new DbService(mServerService.getApplicationContext());
    }

    @Override
    public void onSocketConnected() {

    }

    @Override
    public void onSocketConnectionFailed() {

    }

    @Override
    public void onSendMessageFailed(String socketMessage) {
        mServerHelperObservers.onSendMessageFailed();
    }

    @Override
    public void onUserMessage(UserMessage userMessage) {
        mServerService.writeMessage(new UserMessageReceived(userMessage));
        userMessage.setReceived(true);

        dbService.putUserMessage(userMessage);
        mServerHelperObservers.dbChanged(DBChanged.UserMessageReceived);
        mServerHelperObservers.onUserMessage(userMessage);
    }

    @Override
    public void onUserMessageSent(UserMessageSent userMessageSent) {
        Log.d(TAG, "onUserMessageSent: " + userMessageSent);

        dbService.updateUserMessageSent(userMessageSent);

        mServerHelperObservers.dbChanged(DBChanged.UserMessageSent);
        mServerHelperObservers.onUserMessageSent(userMessageSent);
    }

    @Override
    public void onUserMessageReceived(UserMessageReceived userMessageReceived) {
        Log.d(TAG, "onUserMessageReceived: " + userMessageReceived);
        dbService.updateUserMessageReceived(userMessageReceived);
        mServerHelperObservers.dbChanged(DBChanged.UserMessageSentRecv);
        mServerHelperObservers.onUserMessageReceived(userMessageReceived);
    }

    @Override
    public void onServerUpdate(ServerUpdate serverUpdate){
        mServerHelperObservers.onServerUpdate(serverUpdate);
    }

    @Override
    public void onUserProfile(UserProfile userProfile) {
        Log.d(TAG, "onUserProfile: " + userProfile);
        dbService.putUserProfile(userProfile);
        mServerHelperObservers.dbChanged(DBChanged.UserProfileUpdated);
        mServerHelperObservers.onUserProfile(userProfile);
    }

    @Override
    public void onUserFriendsList(ArrayList<UserFriend> friends) {
        dbService.putNewUserFriendsList(friends);

        mServerHelperObservers.dbChanged(DBChanged.UserFriendsReceived);
        mServerHelperObservers.onUserFriendsList(friends);
    }

    @Override
    public void onUserContactsList(ArrayList<UserContact> contacts) {
        dbService.putNewUserContactsList(contacts);

        mServerHelperObservers.dbChanged(DBChanged.UserContactsReceived);
        mServerHelperObservers.onUserContactsList(contacts);
    }

    @Override
    public  void onAddUserFriend(UserFriend userFriend){
        dbService.putNewUserFriend(userFriend);
        mServerHelperObservers.dbChanged(DBChanged.UserFriendsReceived);
        dbService.putUserSecretKey(""+userFriend.id, userFriend.auth_key, null);
    }

    @Override
    public void onAddUserFriendResponse(AddUserFriend addUserFriend) {
        mServerHelperObservers.onAddUserFriendResponse(addUserFriend);
    }

    @Override
    public void onUpdateUserFriend(UserFriend updateUserFriend) {
        Log.d(TAG, "onUpdateUserFriend "+updateUserFriend);
        dbService.updateUserFriend(updateUserFriend);
        mServerHelperObservers.dbChanged(DBChanged.UserFriendsReceived);
    }


    //    server helper interface
    @Override
    public void addUserFriend(String userData, String type){
        //type is ignored for now
        AddUserFriend addUserFriend = new AddUserFriend();
        addUserFriend.setUserUniqueDetail(userData);
        mServerService.writeMessage(addUserFriend);
    }

    @Override
    public void onAddUserFriendFromToken(String token) {
        String[] splitted = token.split(":");
        String authKey = splitted[0];
        String secretKey = splitted[1];

        AddUserFriendFromToken addUserFriendFromToken = new AddUserFriendFromToken();
        addUserFriendFromToken.setAuthKey(authKey);
        addUserFriendFromToken.setUserId(ApiKeyControllder.getId( mServerService.getApplicationContext() ) );

        mServerService.writeMessage(addUserFriendFromToken);

        dbService.putUserSecretKey(null, authKey,secretKey);
    }

    @Override
    public void sendUserMessageToServer(String toId, String text, String encodedBitmap) {
        try {
            UserMessage userMessage = new UserMessage(text,toId,encodedBitmap,mServerService.getApplicationContext());
            dbService.putSendUserMessageToServer(userMessage);
            mServerHelperObservers.dbChanged(DBChanged.UserMessageSent);

            mServerHelperObservers.onSendUserMessage(userMessage);

            mServerService.writeMessage(userMessage);
        } catch (Exception e) {
            mServerHelperObservers.onSendMessageFailed();
        }
    }

    @Override
    public void sendUserProfilePhotoToServer(String path){
        Log.d(TAG,"sendUserProfilePhotoToServer: "+path);
        UserProfilePicture userProfilePicture = new UserProfilePicture();
        File f = new File(path);
        userProfilePicture.setFilename(f.getName());
        Log.d(TAG,"FileName: "+f.getName());
        int screenWidth = Utils.getScreenWidth(mServerService.getApplicationContext());
        Bitmap scaledBitmap = BitmapUtils.decodeToLowResImage(path,screenWidth,screenWidth);
        Log.d(TAG, "screenWidth: "+screenWidth+" scaledBitmap.width: " + scaledBitmap.getWidth() + " scaledBitmap.height: " + scaledBitmap.getHeight());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        userProfilePicture.setProfile_pic(Base64.encodeToString(byteArray, Base64.DEFAULT));
        mServerService.writeMessage(userProfilePicture);
    }

    @Override
    public void getUserProfile() {
        GetUserProfile socketMessage = new GetUserProfile();
        mServerService.writeMessage(socketMessage);
    }

    @Override
    public void sendUserStatusToServer(String userStatus) {
        SingleSendOperation singleSendOperation = new SingleSendOperation(ServerOperationCodes.user_status, userStatus);
        mServerService.writeMessage(singleSendOperation);
    }

    @Override
    public void sendUserDisplayNameToServer(String displayName) {
        SingleSendOperation singleSendOperation = new SingleSendOperation(ServerOperationCodes.user_display_name, displayName);
        mServerService.writeMessage(singleSendOperation);
    }

    @Override
    public void getFriendsFromServer() {
        GetFriendsList socketMessage = new GetFriendsList();
        mServerService.writeMessage(socketMessage);
    }

    @Override
    public void getContactsFromServer() {
        GetContactsList socketMessage = new GetContactsList();
        mServerService.writeMessage(socketMessage);
    }

    public void addCallback(in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface callback){
        mServerHelperObservers.registerObserver(callback);
    }

    public void removeCallback(in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface callback){
        mServerHelperObservers.unregisterObserver(callback);
    }

    /// helper methods


}
