package in.w4rlock.woosh.service.interfaces;

import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.SocketMessage;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 2/24/15.
 */
public interface ServerSocketInterface {
    void onSocketConnected();
    void onSocketConnectionFailed();
    void onSendMessageFailed(String socketMessage);
    void onUserMessage(UserMessage userMessage);

    void onUserMessageSent(UserMessageSent userMessageSent);

    void onUserFriendsList(ArrayList<UserFriend> friends);
    void onUserContactsList(ArrayList<UserContact> contacts);

    void onUserMessageReceived(UserMessageReceived userMessageReceived);

    void onUserProfile(UserProfile userProfile);
    void onServerUpdate(ServerUpdate serverUpdate);
    void onAddUserFriend(UserFriend userFriend);

    void onAddUserFriendResponse(AddUserFriend addUserFriend);

    void onUpdateUserFriend(UserFriend updateUserFriend);
}
