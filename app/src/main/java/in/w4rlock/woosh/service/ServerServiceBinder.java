package in.w4rlock.woosh.service;

import android.os.Binder;

/**
 * Created by saikrishnak on 2/24/15.
 */
public class ServerServiceBinder extends Binder {
    ServerService serverService;

    public ServerServiceBinder(ServerService serverService) {
        this.serverService = serverService;
    }

    public ServerService getService() {
        return this.serverService;
    }
}
