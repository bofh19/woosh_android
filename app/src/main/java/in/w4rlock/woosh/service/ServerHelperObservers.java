package in.w4rlock.woosh.service;

import java.util.ArrayList;

import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 6/16/15.
 */
public class ServerHelperObservers extends Observers<ServerHelperCallbackInterface> implements ServerHelperCallbackInterface {

    @Override
    public void onSendMessageFailed() {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onSendMessageFailed();
        }
    }

    @Override
    public void dbChanged(DBChanged changeId) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.dbChanged(changeId);
        }
    }

    @Override
    public void onServerUpdate(ServerUpdate serverUpdate) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onServerUpdate(serverUpdate);
        }
    }

    @Override
    public void aDummyMethod() {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.aDummyMethod();
        }
    }

    @Override
    public void onUserMessage(UserMessage userMessage) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserMessage(userMessage);
        }
    }

    @Override
    public void onUserMessageSent(UserMessageSent userMessageSent) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserMessageSent(userMessageSent);
        }
    }

    @Override
    public void onUserMessageReceived(UserMessageReceived userMessageReceived) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserMessageReceived(userMessageReceived);
        }
    }

    @Override
    public void onUserProfile(UserProfile userProfile) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserProfile(userProfile);
        }
    }

    @Override
    public void onUserFriendsList(ArrayList<UserFriend> friends) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserFriendsList(friends);
        }
    }

    @Override
    public void onUserContactsList(ArrayList<UserContact> contacts) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onUserContactsList(contacts);
        }
    }

    @Override
    public void onSendUserMessage(UserMessage userMessage) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if (observer != null)
                observer.onSendUserMessage(userMessage);
        }
    }

    @Override
    public void onAddUserFriendResponse(AddUserFriend addUserFriend) {
        for (ServerHelperCallbackInterface observer:mObservers){
            if(observer != null)
                observer.onAddUserFriendResponse(addUserFriend);
        }
    }
}
