//package in.w4rlock.woosh.service;
//
//import android.os.AsyncTask;
//import android.util.Log;
//
//import org.json.JSONException;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.Socket;
//import java.util.ArrayList;
//import java.util.List;
//
//import in.w4rlock.woosh.service.models.UserContact;
//import in.w4rlock.woosh.service.models.UserFriend;
//import in.w4rlock.woosh.service.models.UserMessage;
//import in.w4rlock.woosh.service.models.UserMessageReceived;
//import in.w4rlock.woosh.service.models.UserMessageSent;
//import in.w4rlock.woosh.utils.ApplicationProperties;
//
///**
// * Created by saikrishnak on 2/24/15.
// */
//public class ServerOld {
//    public static Socket socket;
//    public ServerSocketInterface callback;
//    public static final List<String> socketMessages = new ArrayList<>();
//    private static final String TAG = "SocketServer";
//    private static boolean listenRunning = false;
//    public static String apiKey = "";
//    public ServerOld(){
//
//    }
//
//    DecodeServerMessage.DecodeServerMessageCallback decodeServerMessageCallback =  new DecodeServerMessage.DecodeServerMessageCallback() {
//        @Override
//        public void onPing() {
//            Log.d(TAG,"onPing");
//        }
//
//        @Override
//        public void onPong() {
//            Log.d(TAG,"onPong");
//        }
//
//        @Override
//        public void onUserMessage(UserMessage userMessage) {
//            Log.d(TAG,"onUserMessage "+userMessage);
//            if(callback !=null)
//                callback.onUserMessage(userMessage);
//        }
//
//        @Override
//        public void onUnknownOperation(String message) {
//            Log.d(TAG,"onUnknownOperation "+message);
//        }
//
//        @Override
//        public void onUserFriendsList(ArrayList<UserFriend> friends) {
//            Log.d(TAG,"onUserFriendsList: "+friends);
//            callback.onUserFriendsList(friends);
//        }
//
//        @Override
//        public void onUserContactsList(ArrayList<UserContact> contacts) {
//
//        }
//
//        @Override
//        public void onUserMessageSent(UserMessageSent userMessageSent) {
//            if(callback != null)
//                callback.onUserMessageSent(userMessageSent);
//        }
//
//        @Override
//        public void onUserMessageReceived(UserMessageReceived userMessageReceived) {
//
//        }
//    };
//
//    public void addCallback(ServerSocketInterface callback){
//        this.callback = callback;
//        startServer();
//    }
//
//    public void startServerIfNotRunning(){
//        if(!listenRunning)
//            startServer();
//    }
//
//
//
//    public void startServer() {
//        (new Thread(listenRunnable)).start();
//    }
//
//    public void writeToSocket(String final_message) {
//        socketMessages.add(final_message);
//        startWritingMessages();
//    }
//
//    private static boolean currentlyWriting = false;
//
//    private void startWritingMessages() {
//        Log.d(TAG,"startWritingMessages: "+socketMessages.size()+" currentlyWriting: "+currentlyWriting);
//        if (currentlyWriting) {
//            return;
//        } else {
//            if (socketMessages.size() > 0) {
//                WriteAsync writeAsync = new WriteAsync(socketMessages.remove(0), new WriteAsyncCallback() {
//                    @Override
//                    public void writePreExecute() {
//
//                    }
//
//                    @Override
//                    public void writePostExecute(boolean written, String socketMessage) {
//                        if(!written) {
//                            socketMessages.add(socketMessage);
//                            callback.onSendMessageFailed(socketMessage);
//                        }
//                    }
//                });
//                writeAsync.execute();
//            }
//        }
//    }
//
//    public void forceCloseSocket() {
//        try {
//            socket.close();
//        } catch (Exception e) {
//            Log.d(TAG,"unable to close socket on forceClose "+e.toString());
//            e.printStackTrace();
//        }
//        listenRunning = false;
//    }
//
//    public interface WriteAsyncCallback{
//        void writePreExecute();
//        void writePostExecute(boolean written,String socketMessage);
//    }
//    public static class WriteAsync extends AsyncTask<Void, Void, Void> {
//        private String socketMessage;
//        private WriteAsyncCallback callback;
//        private boolean written = false;
//        public WriteAsync(String socketMessage,WriteAsyncCallback callback) {
//            this.socketMessage = socketMessage;
//            this.callback = callback;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            currentlyWriting = true;
//            callback.writePreExecute();
//        }
//
//        @Override
//        protected void onPostExecute(Void res) {
//            currentlyWriting = false;
//            callback.writePostExecute(written,socketMessage );
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            Log.d(TAG, "Write doInBackground");
//            OutputStream ouStream = null;
//            written = false;
//            try {
//                if (socket == null || socket.isClosed()) {
//                    return null;
//                }
//                ouStream = socket.getOutputStream();
//                String messageString = socketMessage;
//                Log.d(TAG, "messageString: " + messageString);
//                ouStream.write(messageString.getBytes());
//                written = true;
//                ouStream.flush();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }
//
//    public Runnable listenRunnable = new Runnable() {
//        @Override
//        public void run() {
//            listenOnSocket();
//        }
//    };
//
//    public void listenOnSocket() {
//        if(listenRunning)
//            return;
//        Log.d(TAG,"listenOnSocket");
//        if (socket == null || socket.isClosed()) {
//            try {
//                socket = new Socket(ApplicationProperties.SERVER, ApplicationProperties.PORT);
//                listenRunning = true;
//                callback.onSocketConnected();
//                startWritingMessages();
//            } catch (IOException e) {
//                callback.onSocketConnectionFailed();
//                return;
//            }
//        }
//        InputStream inStream;
//        Log.d(TAG,"listenOnSocket: "+socket.isConnected());
//        while (socket.isConnected()) {
//            Log.d(TAG,"trying to read from socket");
//            listenRunning = true;
//            try {
//                inStream = socket.getInputStream();
//
//                byte[] byteBuffer = new byte[26];
//                int numBytes = inStream.read(byteBuffer);
//                byte[] tempBuffer = new byte[numBytes];
//                System.arraycopy(byteBuffer, 0, tempBuffer, 0, numBytes);
//                String message = new String(tempBuffer, "UTF-8");
//                String key = message.substring(0,16);
//                int lengthOfData = Integer.parseInt(message.substring(16,26));
//                Log.d(TAG, "message: " + message + " key: " + key + " lengthOfData: " + lengthOfData);
//                if(key.equals(Server.apiKey)){
//                    byteBuffer = new byte[lengthOfData];
//                    numBytes = inStream.read(byteBuffer);
//                    tempBuffer = new byte[numBytes];
//                    System.arraycopy(byteBuffer, 0, tempBuffer, 0, numBytes);
//                    message = new String(tempBuffer, "UTF-8");
//                    try {
//                        DecodeServerMessage.decodeServerMessage(message, decodeServerMessageCallback);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Log.d(TAG,"failed to parse server message");
//                    }
//                }else{
//                    Log.d(TAG,"Key And ServerKey are not equal Key: "+key+" "+Server.apiKey);
//                    break;
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//                break;
//            }catch (NegativeArraySizeException e){
//                e.printStackTrace();
//                break;
//            }catch (NumberFormatException e){
//                e.printStackTrace();
//                break;
//            }
//        }
//        try {
//            listenRunning = false;
//            callback.onSocketConnectionFailed();
//            socket.close();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//    }
//}