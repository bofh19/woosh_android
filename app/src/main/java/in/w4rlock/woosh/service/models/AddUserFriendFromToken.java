package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * Created by skarumuru on 4/26/2016.
 */
public class AddUserFriendFromToken extends SocketMessage{

    private String authKey;
    private String userId;

    public AddUserFriendFromToken() {
        super();
        this.operation = ServerOperationCodes.add_friend_from_token;
    }

    @Override
    public String toString(){
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<AddUserFriendFromToken>(){}.getType();
        return gson.toJson(this, type);
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
