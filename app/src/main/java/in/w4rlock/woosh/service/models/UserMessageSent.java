package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * Created by saikrishnak on 5/29/15.
 */
public class UserMessageSent extends SocketMessage{

    public String getHash() {
        return hash;
    }

    private String hash;
    private String tstamp;

    public UserMessageSent(String hash){
        this.hash = hash;
        this.operation = ServerOperationCodes.user_message_sent;

        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:MM:ss");
        TimeZone tz = TimeZone.getTimeZone("UTC");
        dateFormatUTC.setTimeZone(tz);
        Date new_date = new Date();
        this.tstamp = (dateFormatUTC.format(new_date)).toString();

    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserMessageSent>() {}.getType();
        return gson.toJson(this, type);
    }

    public static UserMessageSent getFromJson(String json){
        Gson gson = new Gson();
        Type type = new TypeToken<UserMessageSent>() {}.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public String toString(){
        return this.toJson();
    }
}
