package in.w4rlock.woosh.service.interfaces;

import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 5/29/15.
 */
public interface ServerHelperInterface {
    void sendUserMessageToServer(String toId,String text,String encodedBitmap);
    void sendUserProfilePhotoToServer(String path);
    void getUserProfile();
    void sendUserStatusToServer(String userStatus);
    void sendUserDisplayNameToServer(String displayName);
    void getFriendsFromServer();
    void getContactsFromServer();
    void addUserFriend(String userData, String type);
    void onAddUserFriendFromToken(String token);
}
