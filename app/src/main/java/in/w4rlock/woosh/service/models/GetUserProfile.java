package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/9/15.
 */
public class GetUserProfile extends SocketMessage {

    public GetUserProfile() {
        operation = ServerOperationCodes.profile;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<GetUserProfile>() {
        }.getType();
        return gson.toJson(this, type);
    }
}