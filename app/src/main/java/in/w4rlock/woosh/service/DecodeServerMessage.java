package in.w4rlock.woosh.service;

import android.util.Base64;
import android.util.Log;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 5/22/15.
 */
public class DecodeServerMessage {
    private static final String TAG = "DeodeServerMessage";
    public interface DecodeServerMessageCallback{
        void onPing();
        void onPong();
        void onUserMessage(UserMessage userMessage);
        void onUnknownOperation(String message);
        void onUserFriendsList(ArrayList<UserFriend> friends);
        void onUserContactsList(ArrayList<UserContact> contacts);
        void onUserMessageSent(UserMessageSent userMessageSent);
        void onUserMessageReceived(UserMessageReceived userMessageReceived);
        void onServerUpdate(ServerUpdate serverUpdate);
        void onUserProfile(UserProfile userProfile);
        void onAddUserFriend(UserFriend userFriend);
        void onAddUserFriendResponse(AddUserFriend addUserFriend);

        void onUpdateFriend(UserFriend updateUserFriend);
    }

    public static void decodeServerMessage(String encodedMessage, DecodeServerMessageCallback callback) throws JSONException {
        String decodedJson;
        try {
            decodedJson = new String(Base64.decode(encodedMessage, Base64.DEFAULT));
        }catch(Exception e){
            Log.d(TAG,"exception while decoding json "+e.toString());
            return;
        }
        Log.d(TAG,"DecodedJson: "+decodedJson);

        JSONObject thisObj = new JSONObject(decodedJson);
        String operation = thisObj.getString("operation");
        Log.d(TAG, "found operation: "+operation);
        if(operation.equals(ServerOperationCodes.ping)){
            callback.onPing();
        }else if(operation.equals(ServerOperationCodes.pong)){
            callback.onPong();
        }else if(operation.equals(ServerOperationCodes.user_message)){
            callback.onUserMessage(UserMessage.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.friends_list)){
            JSONArray friendsArray = thisObj.getJSONArray(ServerOperationCodes.friends_list);
            ArrayList<UserFriend> friends = new ArrayList<>();
            for(int k=0;k<friendsArray.length();k++){
                friends.add(UserFriend.getFromJson(friendsArray.getJSONObject(k).toString()));
            }
            callback.onUserFriendsList(friends);
        }else if(operation.equals(ServerOperationCodes.contacts_list)){
            JSONArray contactsArray = thisObj.getJSONArray(ServerOperationCodes.contacts_list);
            ArrayList<UserContact> contacts = new ArrayList<>();
            for(int k=0;k<contactsArray.length();k++){
                contacts.add(UserContact.getFromJson(contactsArray.getJSONObject(k).toString()));
            }
            callback.onUserContactsList(contacts);
        }
        else if (operation.equals(ServerOperationCodes.user_message_sent)){
            callback.onUserMessageSent(UserMessageSent.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.user_message_received)){
            callback.onUserMessageReceived(UserMessageReceived.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.profile_pic)){
            callback.onServerUpdate(ServerUpdate.UserProfilePicUpdated);
        }else if(operation.equals(ServerOperationCodes.user_status)){
            callback.onServerUpdate(ServerUpdate.UserStatusUpdated);
        }else if(operation.equals(ServerOperationCodes.user_display_name)){
            callback.onServerUpdate(ServerUpdate.UserDisplayNameUpdated);
        }
        else if(operation.equals(ServerOperationCodes.profile)){
            callback.onUserProfile(UserProfile.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.add_friend)){
            callback.onAddUserFriend(UserFriend.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.add_friend_response)){
            callback.onAddUserFriendResponse(AddUserFriend.getFromJson(thisObj.toString()));
        }else if(operation.equals(ServerOperationCodes.update_friend)){
            callback.onUpdateFriend(UserFriend.getFromJson(thisObj.toString()));
        }
        else{
            callback.onUnknownOperation(thisObj.toString());
        }

    }

}
