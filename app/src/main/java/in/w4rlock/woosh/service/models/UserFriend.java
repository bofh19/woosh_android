package in.w4rlock.woosh.service.models;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

public class UserFriend extends SocketMessage {
    public Long _id;
    public String display_name;
    public int id;
    public String email;
    public String bitmapUrl;
    public String status;

    public String family_name;
    public String name;
    public String given_name;

    public boolean blocked;
    public boolean accepted; // other relation exist ?
    public String invitedBy;
    public String auth_key;
    public boolean secretKeyExchanged;
    public void UserFriend() {
        operation = ServerOperationCodes.friend;
    }

    @Override
    public String toString() {
        return this.toJson();
    }

    @Override
    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserFriend>() {
        }.getType();
        return gson.toJson(this, type);
    }

    public static UserFriend getFromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserFriend>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}