package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

public class PingOperation extends SocketMessage{

        public PingOperation(){
            operation = ServerOperationCodes.ping;
        }

        @Override
        public String toJson() {
            Gson gson = new Gson();
            Type type = new TypeToken<PingOperation>() {}.getType();
            return gson.toJson(this, type);
        }

    @Override
    public String toString(){
        return this.toJson();
    }
}