package in.w4rlock.woosh.service.models;

import android.animation.ValueAnimator;
import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.service.ServerOperationCodes;
import in.w4rlock.woosh.utils.HashingUtility;
import nl.qbusict.cupboard.annotation.Ignore;

public class UserMessage extends SocketMessage {

    public Long _id;
    private String text;
    private String toId;
    private String fromId;
    private String hash;
    private String encodedBitmap;
    private String tstamp;
    private boolean received;
    private boolean sent;
    private boolean viewed;


    public static float timeRemaining(UserMessage um) {
        return (  ((float) (um.getText().length()) / (200.0f*5.0f) ) * 60.0f );
    }

    public UserMessage() {
    }

    public UserMessage(String text, String toId, String encodedBitmap, Context context) throws Exception {
        this.operation = ServerOperationCodes.user_message;
        this.text = text;
        this.toId = toId;
        this.encodedBitmap = encodedBitmap;

        SimpleDateFormat dateFormatUTC = new SimpleDateFormat("MM/dd/yyyy HH:MM:ss");
        TimeZone tz = TimeZone.getTimeZone("UTC");
        dateFormatUTC.setTimeZone(tz);
        Date new_date = new Date();
        this.tstamp = (dateFormatUTC.format(new_date)).toString();
        if (this.text == null)
            this.text = "";
        if (this.encodedBitmap == null)
            this.encodedBitmap = "";

        String hmac_message = tstamp + this.text + this.encodedBitmap;
        String hmac_key = ApiKeyControllder.getApiKey(context);
        this.hash = HashingUtility.HMAC_MD5_encode(hmac_key, hmac_message);

        this.fromId = ApiKeyControllder.getId(context);
    }

    @Override
    public String toString() {
        //return "UserMessage: fromId: " + this.fromId + " toId: " + this.toId + " Text: "+ this.text;
        return this.toJson();
    }

    public String toJson() {
        Gson gson = new Gson();
        Type type = new TypeToken<UserMessage>() {
        }.getType();
        return gson.toJson(this, type);
    }

    public static UserMessage getFromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<UserMessage>() {
        }.getType();
        return gson.fromJson(json, type);
    }



    public String getTstamp() {
        return tstamp;
    }

    public String getEncodedBitmap() {
        return encodedBitmap;
    }

    public String getHash() {
        return hash;
    }

    public String getToId() {
        return toId;
    }

    public String getText() {
        return text;
    }

    public String getFromId() {
        return fromId;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }


}