package in.w4rlock.woosh.service.models;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import in.w4rlock.woosh.service.ServerOperationCodes;

/**
 * Created by saikrishnak on 2/24/15.
 */
public abstract class SocketMessage implements Serializable{
    protected String operation;
    protected String os_type = ServerOperationCodes.os_android;
    protected String os_version = ""+android.os.Build.VERSION.SDK_INT;
    @Override
    public String toString(){
        return " operation: "+operation;
    }

    public abstract String toJson();
    static final int max_length = 10;

    public static String getLengthStringFromLength(Integer length) {
        String len = length + "";
        int no_of_zeros_missing = max_length - len.length();
        if (no_of_zeros_missing <= 0) return len;
        String res = len;
        for (int i = 0; i < no_of_zeros_missing; i++) {
            res = "0" + res;
        }
        return res;
    }

    public static String prepareFinalSocketMessage(String api_key,SocketMessage socketMessage) throws JSONException {
        String json = socketMessage.toJson().toString();
        String encoded = Base64.encodeToString(json.getBytes(), Base64.DEFAULT);
        String length = getLengthStringFromLength(encoded.length());
        String finalMessage = api_key+length+encoded;
        return finalMessage;
    }
}
