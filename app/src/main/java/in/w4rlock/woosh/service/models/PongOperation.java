package in.w4rlock.woosh.service.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import in.w4rlock.woosh.service.ServerOperationCodes;

public class PongOperation extends SocketMessage{

        public PongOperation(){
            operation = ServerOperationCodes.pong;
        }

    @Override
    public String toString(){
        return this.toJson();
    }

        @Override
        public String toJson() {
            Gson gson = new Gson();
            Type type = new TypeToken<PongOperation>() {}.getType();
            return gson.toJson(this, type);
        }
    }