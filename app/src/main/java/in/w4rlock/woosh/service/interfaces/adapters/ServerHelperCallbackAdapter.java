package in.w4rlock.woosh.service.interfaces.adapters;

import java.util.ArrayList;

import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.models.AddUserFriend;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 6/12/15.
 */
public abstract class ServerHelperCallbackAdapter implements ServerHelperCallbackInterface{

    public void onSendMessageFailed(){}
    public void dbChanged(DBChanged changeId){} // when local db changes

    // when server db changes, but not local db
    // means probably either change is incoming
    // or request server for those changes
    public void onServerUpdate(ServerUpdate serverUpdate){}

    public void aDummyMethod(){}

    public void onUserMessage(UserMessage userMessage){}

    public void onUserMessageSent(UserMessageSent userMessageSent){}

    public void onUserMessageReceived(UserMessageReceived userMessageReceived){}

    public void onUserProfile(UserProfile userProfile){}

    public void onUserFriendsList(ArrayList<UserFriend> friends){}

    public void onUserContactsList(ArrayList<UserContact> contacts){}

    public void onSendUserMessage(UserMessage userMessage){}

    public void onAddUserFriendResponse(AddUserFriend addUserFriend){}

}
