package in.w4rlock.woosh;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import in.w4rlock.woosh.controllers.api_controllers.ApiKeyControllder;
import in.w4rlock.woosh.database.db.SocketMessageDatabaseSQLiteOpenHelper;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.fragments.RecentChatFragment;
import in.w4rlock.woosh.fragments.UserContactsFragment;
import in.w4rlock.woosh.fragments.UserFriendsFragment;
import in.w4rlock.woosh.interfaces.OnFragmentInteractionListener;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.ServerServiceBinder;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.utils.Utils;
import nl.qbusict.cupboard.CupboardBuilder;
import nl.qbusict.cupboard.CupboardFactory;
import nl.qbusict.cupboard.DatabaseCompartment;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/4/15.
 */
public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    public static final String FROM_LOGIN = "from_login";
    private static final String TAG = "MainActivity";
    private final Handler mHandler = new Handler();
    ServerService mServerService;
    boolean mBound = false;
    boolean fromLogin = false;
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "attach to service handler called " + mBound);
            if (!mBound) {
                attachToService();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onResume");
        detachFromService();
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG,"onResume");
        attachToService();
    }

    private void detachFromService() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        mServerService = null;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            // remove callbacks here probably
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            Log.d(TAG,"service connected");
            ServerServiceBinder binder = (ServerServiceBinder) service;
            mServerService = binder.getService();
            mBound = true;
        }
    };

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isServerServiceRunning(this)) {
            // binding only if server service is already running
            Intent intent = new Intent(this, ServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mHandler.removeCallbacks(mAttachToService);
        } else {
            // we have to start it and get binded
            Log.d(TAG,"service not running starting and adding callback");
            Intent serviceIntent;
            if(fromLogin)
                serviceIntent = new Intent(ServerService.ACTION_RECONNECT_SOCKET);
            else
                serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            startService(serviceIntent);
            mHandler.postDelayed(mAttachToService,500);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fromLogin = getIntent().getBooleanExtra(FROM_LOGIN,false);

        final ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        if(ApiKeyControllder.getApiKey(getApplicationContext()) == null){
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }
        initializeCupboardEntities();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_logout:
                executeLogout();
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(RecentChatFragment.newInstance(), getString(R.string.main_tabname_recent));
        adapter.addFragment(UserFriendsFragment.newInstance(), getString(R.string.main_tabname_friends));
        adapter.addFragment(UserContactsFragment.newInstance(),getString(R.string.main_tabname_contacts));
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    /*
    initial database registering
     */

    void initializeCupboardEntities(){
        CupboardFactory.setCupboard(new CupboardBuilder().useAnnotations().build());
        SocketMessageDatabaseSQLiteOpenHelper.register();
    }


    /*
        LOGOUT PROC
     */
    private void executeLogout(){
        ApiKeyControllder.clearApiKey(getApplicationContext());
        mServerService.executeLogout();
        (new ClearDB()).execute();
    }

    class ClearDB extends AsyncTask<Void,Void,Void>{
        ProgressDialog dialog;

        @Override
        protected void onPreExecute(){
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setCancelable(false);
            dialog.setMessage(getString(R.string.main_logout_in_progress));
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            clearDBDone();
        }

        @Override
        protected Void doInBackground(Void... params) {
            int clean_counter = 0;
            SQLiteDatabase db = SocketMessageDb.getWriteDatabase(getApplicationContext());
            DatabaseCompartment dbc = cupboard().withDatabase(db);
            Cursor userFriends = cupboard().withDatabase(db).query(UserFriend.class).getCursor();
            QueryResultIterable<UserFriend> itr = cupboard().withCursor(userFriends).iterate(UserFriend.class);
            for(UserFriend userFriend: itr){
                dbc.delete(userFriend);
                clean_counter++;
            }

            Cursor userContacts = cupboard().withDatabase(db).query(UserContact.class).getCursor();
            QueryResultIterable<UserContact> itr2 = cupboard().withCursor(userContacts).iterate(UserContact.class);
            for(UserContact userContact: itr2){
                dbc.delete(userContact);
                clean_counter++;
            }

            Cursor userMessages = cupboard().withDatabase(db).query(UserMessage.class).getCursor();
            QueryResultIterable<UserMessage> itr3 = cupboard().withCursor(userMessages).iterate(UserMessage.class);
            for (UserMessage userMessage : itr3) {
                dbc.delete(userMessage);
                clean_counter++;
            }

            db.close();
            Log.d(TAG,"clean_counter: "+clean_counter++);
            return null;
        }
    }

    private void clearDBDone(){

        startActivity(new Intent(this, LoginActivity.class));
        if(Utils.isServerServiceRunning(getApplicationContext())){
            detachFromService();
            Intent serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            stopService(serviceIntent);
        }
        this.finish();
    }

    /*
        fragment interaction listener
     */

    public ServerService getServerService(){
        return mServerService;
    }
}
