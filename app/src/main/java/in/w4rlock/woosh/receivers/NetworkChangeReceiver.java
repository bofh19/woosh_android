package in.w4rlock.woosh.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.utils.Utils;

/**
 * Created by saikrishnak on 5/27/15.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isAvailable() || mobile.isAvailable()) {
            Log.d("NetworkChangeReceiver", "Flag No 1");
            if(Utils.isOnline(context)){
                Log.d("NetworkChangeReceiver", "online: ");
                Intent serviceIntent = new Intent(ServerService.ACTION_RECONNECT_SOCKET);
                serviceIntent.setPackage(ServerService.PACKAGE_NAME);
                context.startService(serviceIntent);
            }else{
                Log.d("NetworkChangeReceiver","not online");
            }
        }
    }
}
