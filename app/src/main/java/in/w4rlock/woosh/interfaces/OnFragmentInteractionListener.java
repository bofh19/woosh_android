package in.w4rlock.woosh.interfaces;

import in.w4rlock.woosh.service.ServerService;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public interface OnFragmentInteractionListener {
    ServerService getServerService();
}
