package in.w4rlock.woosh.interfaces;

/**
 * Created by saikrishnak on 6/12/15.
 */
public enum ServerUpdate {
    UserProfilePicUpdated, UserDisplayNameUpdated, UserStatusUpdated
}
