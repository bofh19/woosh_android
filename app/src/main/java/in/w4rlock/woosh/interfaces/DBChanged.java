package in.w4rlock.woosh.interfaces;

/**
 * w4rlock scripts
 * Created by saikrishnak on 6/5/15.
 */
public enum DBChanged {
    UserMessageSent, UserMessageReceived, UserFriendsReceived, UserContactsReceived, UserMessageSentRecv, UserProfileUpdated
}
