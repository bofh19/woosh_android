package in.w4rlock.woosh;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.fragments.ChatWithXFragment;
import in.w4rlock.woosh.interfaces.OnFragmentInteractionListener;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.ServerServiceBinder;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.utils.ApplicationProperties;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class ChatWithXActivity extends AppCompatActivity implements OnFragmentInteractionListener {
    private static final String TAG = "ChatWithXActivity";
    public static final String CHAT_WITH_ID = "chat_with_id";
    private final Handler mHandler = new Handler();
    ServerService mServerService;
    boolean mBound = false;
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "attach to service handler called " + mBound);
            if (!mBound) {
                attachToService();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        detachFromService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        attachToService();
    }

    private void detachFromService() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        mServerService = null;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            // remove callbacks here probably
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            Log.d(TAG, "service connected");
            ServerServiceBinder binder = (ServerServiceBinder) service;
            mServerService = binder.getService();
            mBound = true;
        }
    };

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isServerServiceRunning(this)) {
            // binding only if server service is already running
            Intent intent = new Intent(this, ServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mHandler.removeCallbacks(mAttachToService);
        } else {
            // we have to start it and get binded
            Log.d(TAG, "service not running starting and adding callback");
            Intent serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            startService(serviceIntent);
            mHandler.postDelayed(mAttachToService, 500);
        }
    }

    private UserFriend userFriend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_x);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final ActionBar ab = getSupportActionBar();
//        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
//        ab.setLogo(R.mipmap.ic_launcher);
//        ab.setDisplayHomeAsUpEnabled(true);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                L.d(TAG,"toolbar clicked");
            }
        });


        int chatId = getIntent().getExtras().getInt(ChatWithXActivity.CHAT_WITH_ID, -1);
        Log.d(TAG, "" + chatId);
        if (chatId == -1) {
            finish();
        }

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, ChatWithXFragment.newInstance(chatId))
                .commit();
        SQLiteDatabase db = SocketMessageDb.getReadDatabase(getApplicationContext());
        userFriend = cupboard().withDatabase(db).query(UserFriend.class).withSelection(" id = ?", ""+chatId).get();
        Log.d(TAG,"userFriend: "+userFriend);
        if (userFriend == null){
            finish();
        }
        ab.setTitle(userFriend.display_name);
        int actionBarDimen = Utils.getPixelsFromDp(getApplicationContext(),32);
        Glide.with(this).load(ApplicationProperties
                .getBitmapFullUrl(userFriend.bitmapUrl)).asBitmap().centerCrop()
                .into(new SimpleTarget<Bitmap>(actionBarDimen, actionBarDimen) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        ab.setLogo(new BitmapDrawable(getResources(), resource));
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat_with_x, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /*
        fragment interaction listener
     */

    public ServerService getServerService() {
        return mServerService;
    }
}
