package in.w4rlock.woosh;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.imagepick.ImagePickerActivity;
import in.w4rlock.woosh.interfaces.DBChanged;
import in.w4rlock.woosh.interfaces.ServerUpdate;
import in.w4rlock.woosh.service.ServerHelper;
import in.w4rlock.woosh.service.interfaces.ServerHelperCallbackInterface;
import in.w4rlock.woosh.service.interfaces.ServerHelperInterface;
import in.w4rlock.woosh.service.ServerService;
import in.w4rlock.woosh.service.ServerServiceBinder;
import in.w4rlock.woosh.service.interfaces.adapters.ServerHelperCallbackAdapter;
import in.w4rlock.woosh.service.models.UserProfile;
import in.w4rlock.woosh.utils.ApplicationProperties;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = "SettingsActivity";
    private static final int SELECT_PHOTO = 100;
    private ServerHelper mServerHelper;
    private final Handler mHandler = new Handler();
    ServerService mServerService;
    boolean mBound = false;
    boolean mUpdateProfile = false;
    private final Runnable mAttachToService = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "attach to service handler called " + mBound);
            if (!mBound) {
                attachToService();
            }
        }
    };

    @Override
    protected void onDestroy(){
        super.onDestroy();
        detachFromService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onResume");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG,"onResume");
    }

    private void detachFromService() {
        // Unbind from the service
        mServerHelper.removeCallback(mServerCallback);
        mServerHelper = null;
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        mServerService = null;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
            // remove callbacks here probably
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            Log.d(TAG,"service connected");
            ServerServiceBinder binder = (ServerServiceBinder) service;
            mServerService = binder.getService();
            mBound = true;
            mServerHelper = mServerService.getServerHelper();
            mServerHelper.addCallback(mServerCallback);
            if (mUpdateProfile){
                mServerHelper.getUserProfile();
                mUpdateProfile = false;
            }
        }
    };

    private void attachToService() {
        // Bind to LocalService
        if (Utils.isServerServiceRunning(this)) {
            // binding only if server service is already running
            Intent intent = new Intent(this, ServerService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            mHandler.removeCallbacks(mAttachToService);
        } else {
            // we have to start it and get binded
            Log.d(TAG,"service not running starting and adding callback");
            Intent serviceIntent = new Intent(ServerService.START_SERVICE);
            serviceIntent.setPackage(ServerService.PACKAGE_NAME);
            startService(serviceIntent);
            mHandler.postDelayed(mAttachToService,500);
        }
    }

    class ServerHelperCallback extends ServerHelperCallbackAdapter{
        @Override
        public void dbChanged(DBChanged changeId) {
            if(changeId == DBChanged.UserProfileUpdated){
                Utils.showSnackbar(findViewById(R.id.edit_image_fab),"Profile Updated");
                final UserProfile userProfile = cupboard()
                        .withDatabase(SocketMessageDb.getReadDatabase(getApplicationContext()))
                        .query(UserProfile.class).orderBy("_id desc").limit(1).get();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadBackdrop(userProfile);
                    }
                });
                L.d(TAG, "UserProfile: "+userProfile);
            }
        }
    }

    private ServerHelperCallback mServerCallback = new ServerHelperCallback();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        attachToService();
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Settings");

        UserProfile userProfile = cupboard()
                .withDatabase(SocketMessageDb.getReadDatabase(getApplicationContext()))
                .query(UserProfile.class).orderBy("_id desc").limit(1).get();

        loadBackdrop(userProfile);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.edit_image_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent photoPickerIntent = new Intent(SettingsActivity.this, ImagePickerActivity.class);

                SettingsActivity.this.startActivityForResult(photoPickerIntent, SELECT_PHOTO);


            }
        });
        mUpdateProfile = true;
        setUpButtonListeners();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadBackdrop(UserProfile userProfile) {
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        final EditText displayName = (EditText) findViewById(R.id.display_name);
        final EditText userStatus = (EditText) findViewById(R.id.user_status);
        if (userProfile != null){
            if(userProfile.bitmapUrl != null)
                Glide.with(this).load(ApplicationProperties.getBitmapFullUrl(userProfile.bitmapUrl)).centerCrop().into(imageView);
            else
                Glide.with(this).load(R.drawable.android).centerCrop().into(imageView);
            displayName.setText(userProfile.display_name);
            userStatus.setText(userProfile.user_status);
        }else {
            Glide.with(this).load(R.drawable.android).centerCrop().into(imageView);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case SELECT_PHOTO:
                if (data != null &&data.hasExtra("selectedItems")) {
                    ArrayList<String> selectedItems = data.getExtras().getStringArrayList("selectedItems");
                    L.d(TAG,"selectedItems: "+selectedItems);
                    if(selectedItems.size() >= 1){
                        mServerService.getServerHelper().sendUserProfilePhotoToServer(selectedItems.get(0));
                    }
                }else{
                    Utils.showSnackbar(findViewById(R.id.edit_image_fab),"Canceled");
                }
        }
    }

    private void updateUserDisplayName(){
        if(mServerHelper == null){
            attachToService();
            Utils.showSnackbar(findViewById(R.id.edit_image_fab),getString(R.string.failed_to_update_try_again));
        }else{
            final EditText displayName = (EditText) findViewById(R.id.display_name);
            mServerHelper.sendUserDisplayNameToServer(displayName.getText().toString());
        }
    }

    private void updateUserStatus() {
        if(mServerHelper == null){
            attachToService();
            Utils.showSnackbar(findViewById(R.id.edit_image_fab),getString(R.string.failed_to_update_try_again));
        }else{
            final EditText userStatus = (EditText) findViewById(R.id.user_status);
            mServerHelper.sendUserStatusToServer(userStatus.getText().toString());
        }
    }

    private void enableEditTextView(EditText editText){
        editText.setEnabled(true);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setFocusable(true);
        editText.setTextColor(Color.BLACK);
    }

    private void disableEditTextView(EditText editText){
        editText.setEnabled(false);
        editText.setInputType(InputType.TYPE_NULL);
        editText.setFocusable(false);
        editText.setTextColor(Color.BLACK);
    }

    private void setUpButtonListeners(){
        setUpButtonListeners((ImageButton)findViewById(R.id.display_name_edit),
                (ImageButton)findViewById(R.id.display_name_done),
                (ImageButton)findViewById(R.id.display_name_cancel),
                (EditText) findViewById(R.id.display_name),BUTTON_UPDATE.DISPLAY_NAME);

        setUpButtonListeners((ImageButton)findViewById(R.id.user_status_edit),
                (ImageButton)findViewById(R.id.user_status_done),
                (ImageButton)findViewById(R.id.user_status_cancel),
                (EditText) findViewById(R.id.user_status),BUTTON_UPDATE.USER_STATUS);
        findViewById(R.id.display_name_cancel).performClick();
        findViewById(R.id.user_status_cancel).performClick();
    }

    private enum BUTTON_UPDATE{DISPLAY_NAME, USER_STATUS}

    private void setUpButtonListeners(final ImageButton editButton,final ImageButton doneButton,
                                      final ImageButton cancelButton, final EditText editText, final BUTTON_UPDATE button_update){
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                doneButton.setVisibility(View.VISIBLE);
                enableEditTextView(editText);
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.GONE);
                doneButton.setVisibility(View.GONE);
                disableEditTextView(editText);
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.GONE);
                doneButton.setVisibility(View.GONE);
                disableEditTextView(editText);
                if(button_update == BUTTON_UPDATE.DISPLAY_NAME)
                    updateUserDisplayName();
                else if(button_update == BUTTON_UPDATE.USER_STATUS)
                    updateUserStatus();
            }
        });
    }
}
