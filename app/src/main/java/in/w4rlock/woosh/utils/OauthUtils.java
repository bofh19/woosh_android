package in.w4rlock.woosh.utils;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

public class OauthUtils {
	public static final String TAG = "OauthUtils";
	public static interface ActivityCallback {
		public void onLoadingStarted();

		public void onLoadingEnded(String token);

		public void handleException(
				UserRecoverableAuthException userRecoverableAuthException);

		public Activity getActivity();
	}

	public static class ClearToken extends AsyncTask<Void,Void,Void>{

		private final String token;
		private final Context context;

		public ClearToken(Context context, String token){
			this.context = context;
			this.token = token;
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				GoogleAuthUtil.clearToken(context,token);
			} catch (GoogleAuthException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public static class GetUsernameTask extends AsyncTask<Void,Void,String> {
		ActivityCallback mActivity;
		String mScope;
		String mEmail;

		public GetUsernameTask(ActivityCallback activity, String name, String scope) {
			this.mActivity = activity;
			this.mScope = scope;
			this.mEmail = name;
		}
		

		@Override
		protected void onPostExecute(String result) {
			mActivity.onLoadingEnded(result);
		}


		/**
		 * Executes the asynchronous job. This runs when you call execute() on
		 * the AsyncTask instance.
		 */
		@Override
		protected String doInBackground(Void... params) {
			Log.d(TAG, "getting token");
			try {
				String token = fetchToken();
				if (token != null) {
					Log.d(TAG, token);
					return token;
				} else {
					Log.d(TAG, "is null");
				}
			} catch (IOException e) {
				Log.d(TAG, e.toString());
			}
			return null;
		}

		/**
		 * Gets an authentication token from Google and handles any
		 * GoogleAuthException that may occur.
		 */
		protected String fetchToken() throws IOException {
			try {
				return GoogleAuthUtil.getToken(mActivity.getActivity(), mEmail,mScope);
			} catch (UserRecoverableAuthException userRecoverableException) {
				(mActivity).handleException(userRecoverableException);
				Log.d(TAG, userRecoverableException.toString());
			} catch (GoogleAuthException fatalException) {
				Log.d(TAG, fatalException.toString());
			}
			return null;
		}
	}
}
