package in.w4rlock.woosh.utils;

/**
 * Created by saikrishnak on 2/24/15.
 */
public class ApplicationProperties {
        public static final String SERVER = "192.168.1.16";
//    public static final String SERVER = "172.22.1.213";
   // public static final String SERVER = "rmrf.w4rlock.in";
    public static final int PORT = 6070;


    //    variable prefs
    public static final String pref = "in.w4rlock.woosh";
    public static final String apiKeyPref = ApplicationProperties.pref + ".api_key";
    public static final String apiUserId = ApplicationProperties.pref + ".api_user_id";

    //    network prefs
    public static final String domain = "http://"+SERVER + ":8000";

//    public static final String domain = "http://rmrf.w4rlock.in";
    private static final String oauth_url = "/api/auth/2/?access_token=";

    public static String getOauthUrl(final String access_token) {
        return domain + oauth_url + access_token;
    }

    public static String getBitmapFullUrl(String url){
        if(url != null){
            return domain+url;
        }else{
            return null;
        }
    }
}