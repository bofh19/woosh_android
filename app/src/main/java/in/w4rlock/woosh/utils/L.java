package in.w4rlock.woosh.utils;

import android.util.Log;

/**
 * Created by saikrishnak on 16/02/14.
 */
public class L {

    public static void d(Object object, String s) {
        Log.d(object.getClass().toString(), s);
    }

    public static void d(Class clazz, String s) {
        Log.d(clazz.toString(), s);
    }

    public static void d(String DEBUG_TAG, String s) {
        Log.d(DEBUG_TAG, s);
    }

    public static void e(String DEBUG_TAG, String s) {
        Log.e(DEBUG_TAG, s);
    }

    public static void w(String string) {
        Log.w("AUTO_NO_TAG", string);
    }

    public static void i(String string) {
        Log.i("AUTO_NO_TAG", string);
    }
}