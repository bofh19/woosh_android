package in.w4rlock.woosh;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.w4rlock.woosh.controllers.ChatListRecyclerAdapter;
import in.w4rlock.woosh.database.db.SocketMessageDb;
import in.w4rlock.woosh.fragments.ChatFragment;
import in.w4rlock.woosh.service.models.UserFriend;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;


public class ChatListFragment extends Fragment {

    private static final String TAG="ChatListFragment";

    public static ArrayList<UserFriend> friends = new ArrayList<>();
    private ChatListRecyclerAdapter chatListRecyclerAdapter;
    private RecyclerView chatListPersonsView;
    private LinearLayoutManager layoutManager;
    private Activity activity;
    public ChatListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chat_list, container, false);

        chatListPersonsView = (RecyclerView)rootView.findViewById(R.id.chat_list_persons);

        chatListPersonsView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatListPersonsView.setLayoutManager(layoutManager);

        chatListRecyclerAdapter = new ChatListRecyclerAdapter(getActivity().getApplicationContext(), mRecycleViewCallback);

        chatListPersonsView.setAdapter(chatListRecyclerAdapter);

        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(getActivity().getApplicationContext());
        Cursor userFriends = cupboard().withDatabase(db).query(UserFriend.class).getCursor();
        QueryResultIterable<UserFriend> itr = cupboard().withCursor(userFriends).iterate(UserFriend.class);
        for(UserFriend friend: itr){
            chatListRecyclerAdapter.addItem(friend);
        }
        db.close();
        return rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    ChatListRecyclerAdapter.RecyclerItemSelected mRecycleViewCallback  = new ChatListRecyclerAdapter.RecyclerItemSelected() {
        @Override
        public void onItemSelected(UserFriend friend) {
            ChatFragment.chatingWithId = friend.id;
//            ((ChatList)activity).changeFragmentTo(ChatList.CHAT_FRAGMENT);
        }
    };
}
