package in.w4rlock.woosh.network;

import android.os.AsyncTask;
import android.util.Log;

import in.w4rlock.woosh.utils.ApplicationProperties;

/**
 * Created by saikrishnak on 5/19/15.
 */
public class GetApiKeyFromToken {
    private static final String TAG="GetApiKeyFromToken";
    public static String getApiKey(String access_token) throws Exception{
        GetUrlRawData getUrlRawData = new GetUrlRawData();
        return getUrlRawData.getData(ApplicationProperties.getOauthUrl(access_token));
    }

    public static class AsyncTasker extends AsyncTask<String, Void, String> {
        @Override
        protected void onPostExecute(String result) {
            this.callback.onLoadingEnded();

            this.callback.onResult(result);
        }

        @Override
        protected void onPreExecute() {
            this.callback.onLoadingStarted();
        }

        private AsyncTaskInterface<String> callback;

        public AsyncTasker(AsyncTaskInterface<String> callback){
            this.callback = callback;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return getApiKey(params[0]);
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
            return null;
        }

    }
}
