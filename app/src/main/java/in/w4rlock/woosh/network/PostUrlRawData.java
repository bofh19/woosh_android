package in.w4rlock.woosh.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;



public class PostUrlRawData {
	private static final String DEBUG_TAG = PostUrlRawData.class.toString();

	public static String postData(ArrayList<KeyValue> pairs, String url) throws Exception {
		Log.d(DEBUG_TAG, "Fetching: " + url);
		long start_time = System.nanoTime();
        URL url_internal = new URL(url);
        HttpURLConnection conn = (HttpURLConnection)url_internal.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(os, "UTF-8"));
        writer.write(getQuery(pairs));
        writer.flush();
        writer.close();
        os.close();
        conn.connect();

        InputStream is = conn.getInputStream();
        String contentAsString = readAll(is);

		long end_time = System.nanoTime();
		double difference = (end_time - start_time) / 1e6;
		Log.d(DEBUG_TAG, "Success(Fetched In: " + difference + " " + url);
		return contentAsString;
	}

	private static String readAll(InputStream stream) throws Exception {
		StringBuilder sb = new StringBuilder();
		String s;
		Reader reader = new InputStreamReader(stream, "UTF-8");
		BufferedReader buf = new BufferedReader(reader);
		while (true) {
			s = buf.readLine();
			if (s == null || s.length() == 0)
				break;
			sb.append(s);
		}
		return sb.toString();
	}

    private static String getQuery(List<KeyValue> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (KeyValue pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.value, "UTF-8"));
        }

        return result.toString();
    }
}
