package in.w4rlock.woosh.network;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

public class GetUrlRawData {
    private static final String DEBUG_TAG = GetUrlRawData.class.toString();
    private int readTimeOut = 200000;
    private int connTimeOut = 300000;
    private int responseCode = -1;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getReadTimeOut() {
        return readTimeOut;
    }

    public int getConnTimeOut() {
        return connTimeOut;
    }

    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    public void setConnTimeOut(int connTimeOut) {
        this.connTimeOut = connTimeOut;
    }

    public String getData(String strurl) throws Exception {
        Log.d(DEBUG_TAG, "Fetching: " + strurl);
        long start_time = System.nanoTime();
        URL url = new URL(strurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(getReadTimeOut());
        conn.setConnectTimeout(getConnTimeOut());
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();
        setResponseCode(conn.getResponseCode());
        InputStream is = conn.getInputStream();
        String contentAsString = readAll(is);
        long end_time = System.nanoTime();
        double difference = (end_time - start_time) / 1e6;
        Log.d(DEBUG_TAG, "Success(Fetched In: " + difference + " " + strurl);
        return contentAsString;
    }

    private String readAll(InputStream stream) throws Exception {
        StringBuilder sb = new StringBuilder();
        String s;
        Reader reader = new InputStreamReader(stream, "UTF-8");
        BufferedReader buf = new BufferedReader(reader);
        while (true) {
            s = buf.readLine();
            if (s == null || s.length() == 0)
                break;
            sb.append(s);
        }
        return sb.toString();
    }
}