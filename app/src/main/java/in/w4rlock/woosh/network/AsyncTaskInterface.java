package in.w4rlock.woosh.network;

/**
 * Created by saikrishnak on 5/19/15.
 */
public interface AsyncTaskInterface<T> {
    public void onLoadingStarted();

    public void onLoadingEnded();

    public void onProgressUpdate(int percent);

    public void onError(Exception e);

    public void onResult(T t);
}
