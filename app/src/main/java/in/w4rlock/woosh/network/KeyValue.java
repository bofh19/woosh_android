package in.w4rlock.woosh.network;

/**
 * Created by saikrishnak on 5/19/15.
 */
public class KeyValue {
    public String key;
    public String value;
    public KeyValue(String key,String value){
        this.key = key;
        this.value = value;
    }
}
