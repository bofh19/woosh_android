package in.w4rlock.woosh.database.db;

import android.content.Context;

import java.util.ArrayList;

import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;

/**
 * Created by saikrishnak on 7/8/15.
 */
public interface DbServiceInt {
    void putUserMessage(UserMessage userMessage);

    void updateUserMessageSent(UserMessageSent userMessageSent);

    void updateUserMessageReceived(UserMessageReceived userMessageReceived);

    void putUserProfile(UserProfile userProfile);

    void putNewUserFriendsList(ArrayList<UserFriend> friends);

    void putNewUserContactsList(ArrayList<UserContact> contacts);

    void putSendUserMessageToServer(UserMessage userMessage);
    void putNewUserFriend(UserFriend userFriend);

    String getUserSecretKey(UserFriend userFriend);

    void putUserSecretKey(String userId, String authToken, String secretKey);
    void updateUserFriend(UserFriend updateUserFriend);
}
