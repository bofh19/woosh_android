package in.w4rlock.woosh.database.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import in.w4rlock.woosh.models.UserSecretKey;
import in.w4rlock.woosh.service.models.SocketMessage;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserMessageReceived;
import in.w4rlock.woosh.service.models.UserMessageSent;
import in.w4rlock.woosh.service.models.UserProfile;
import in.w4rlock.woosh.utils.L;
import in.w4rlock.woosh.utils.Utils;
import nl.qbusict.cupboard.DatabaseCompartment;
import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by saikrishnak on 7/8/15.
 */
public class DbService implements DbServiceInt {
    public static final String TAG = "DbService";
    private Context context;
    public DbService(Context context){
        this.context = context;
    }

    public void putUserMessage(UserMessage userMessage){
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        cupboard().withDatabase(db).put(userMessage);
        db.close();
    }

    @Override
    public void updateUserMessageSent(UserMessageSent userMessageSent) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        ContentValues values = new ContentValues(1);
        values.put("sent",true);
        int updated = cupboard().withDatabase(db).update(UserMessage.class, values, "hash = ?", userMessageSent.getHash());
        L.d(TAG, "updateUserMessageSent updated: "+updated);
        db.close();
    }

    @Override
    public void updateUserMessageReceived(UserMessageReceived userMessageReceived) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        ContentValues values = new ContentValues(1);
        values.put("received",true);
        cupboard().withDatabase(db).update(UserMessage.class, values, "hash = ?", userMessageReceived.getHash());
        db.close();
    }

    @Override
    public void putUserProfile(UserProfile userProfile) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        cupboard().withDatabase(db).put(userProfile);
        db.close();
    }

    @Override
    public void putNewUserFriendsList(ArrayList<UserFriend> friends) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        DatabaseCompartment dbc = cupboard().withDatabase(db);
        Cursor userFriends = cupboard().withDatabase(db).query(UserFriend.class).getCursor();
        QueryResultIterable<UserFriend> itr = cupboard().withCursor(userFriends).iterate(UserFriend.class);

        for(UserFriend userFriend: itr){
            dbc.delete(userFriend);
        }

        for(UserFriend userFriend:friends){
            dbc.put(userFriend);
        }
        db.close();
    }

    @Override
    public void putNewUserFriend(UserFriend userFriend){
        // add or update
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        DatabaseCompartment dbc = cupboard().withDatabase(db);
        UserFriend dbUser = cupboard().withDatabase(db).query(UserFriend.class).withSelection("id = ?", "" + userFriend.id).get();
        if(dbUser == null){
            dbc.put(userFriend);
        }else{
            userFriend._id = dbUser._id;
            userFriend.id = dbUser.id;
            cupboard().withDatabase(db).put(userFriend);
        }
        db.close();
    }

    @Override
    public void putNewUserContactsList(ArrayList<UserContact> contacts) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        DatabaseCompartment dbc = cupboard().withDatabase(db);

        Cursor userContacts = cupboard().withDatabase(db).query(UserContact.class).getCursor();
        QueryResultIterable<UserContact> itr = cupboard().withCursor(userContacts).iterate(UserContact.class);
        for(UserContact userContact: itr){
            dbc.delete(userContact);
        }

        for(UserContact userContact:contacts){
//            Log.d(TAG,"userContact: "+userContact);
            dbc.put(userContact);
        }
        db.close();
    }

    @Override
    public void putSendUserMessageToServer(UserMessage userMessage) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        cupboard().withDatabase(db).put(userMessage);
        db.close();
    }

    @Override
    public String getUserSecretKey(UserFriend userFriend) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        DatabaseCompartment dbc = cupboard().withDatabase(db);
        try {
            UserSecretKey secretKey = cupboard().withDatabase(db).query(UserSecretKey.class).withSelection("id = ?", "" + userFriend.id).get();

            if (secretKey == null || secretKey.secretKey == null || secretKey.secretKey.length() <= 0) {
                cupboard().withDatabase(db).delete(UserSecretKey.class, "id = ?", ""+userFriend.id);
                secretKey = new UserSecretKey();
                secretKey.id = "" + userFriend.id;
                secretKey.authToken = userFriend.auth_key;
                secretKey.secretKey = Utils.getRandomString(128);
                dbc.put(secretKey);
                return secretKey.secretKey;
            } else {
                return secretKey.secretKey;
            }
        }finally {
            db.close();
        }
    }

    @Override
    public void putUserSecretKey(String userId, String authToken, String secretKey){
        // used when we get secret key and auth token from the camera scan
        // used when we get auth token and id from add user friend response
        if (userId == null && secretKey == null){
            // we wont do anything if both of these are null
            return;
        }
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        DatabaseCompartment dbc = cupboard().withDatabase(db);

        UserSecretKey userSecretKey = cupboard().withDatabase(db).query(UserSecretKey.class).withSelection("authToken = ?", authToken).get();
        if (userSecretKey == null) {
            if(userId == null){
                userId = "";
            }
            UserSecretKey userSecretKey2 = new UserSecretKey();
            userSecretKey2.id = "" + userId;
            userSecretKey2.authToken = authToken;
            userSecretKey2.secretKey = secretKey;
            dbc.put(userSecretKey2);
        } else {
            if (userId == null){
                // if userId is null update secret key
                userSecretKey.secretKey = secretKey;
                dbc.put(userSecretKey);
            }else{
                // if user id is not null then update only id
                userSecretKey.id = userId;
                userSecretKey.authToken = authToken;
                dbc.put(userSecretKey);
            }
        }
        db.close();
    }

    @Override
    public void updateUserFriend(UserFriend updateUserFriend) {
        SQLiteDatabase db = SocketMessageDb.getWriteDatabase(context);
        ContentValues values = new ContentValues(1);
        values.put("received",true);
        UserFriend dbUserFriend = cupboard().withDatabase(db).query(UserFriend.class).withSelection("id = ?", ""+updateUserFriend.id).get();
        if(dbUserFriend != null){
            updateUserFriend._id = dbUserFriend._id;
        }
        DatabaseCompartment dbc = cupboard().withDatabase(db);
        dbc.put(updateUserFriend);
        db.close();
    }

}
