package in.w4rlock.woosh.database.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by saikrishnak on 5/19/15.
 */
public class SocketMessageDb {
    public static SQLiteDatabase getWriteDatabase(Context context) {
        SocketMessageDatabaseSQLiteOpenHelper sqliteHelper = new SocketMessageDatabaseSQLiteOpenHelper(context);
        return sqliteHelper.getWritableDatabase();
    }

    public static SQLiteDatabase getReadDatabase(Context context) {
        SocketMessageDatabaseSQLiteOpenHelper sqliteHelper = new SocketMessageDatabaseSQLiteOpenHelper(context);
        return sqliteHelper.getReadableDatabase();
    }
}
