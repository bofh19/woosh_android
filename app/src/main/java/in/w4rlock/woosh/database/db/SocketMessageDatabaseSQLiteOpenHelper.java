package in.w4rlock.woosh.database.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import in.w4rlock.woosh.models.UserSecretKey;
import in.w4rlock.woosh.service.models.SocketMessage;
import in.w4rlock.woosh.service.models.UserContact;
import in.w4rlock.woosh.service.models.UserFriend;
import in.w4rlock.woosh.service.models.UserMessage;
import in.w4rlock.woosh.service.models.UserProfile;
import nl.qbusict.cupboard.Cupboard;
import nl.qbusict.cupboard.CupboardBuilder;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;
/**
 * Created by saikrishnak on 5/19/15.
 */
public class SocketMessageDatabaseSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "whoosh_messages.db";
    private static final int DATABASE_VERSION = 4;

    static {
        // register our models
//        cupboard().register(UserMessage.class);
//        cupboard().register(UserFriend.class);
//        cupboard().register(UserContact.class);
//        cupboard().register(UserProfile.class);
        register();
    }

    public SocketMessageDatabaseSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // this will ensure that all tables are created
        Cupboard annotatedCupboard = new CupboardBuilder(cupboard()).useAnnotations().build();
        annotatedCupboard.withDatabase(db).createTables();
        // add indexes and other database tweaks
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // this will upgrade tables, adding columns and new tables.
        // Note that existing columns will not be converted
        Cupboard annotatedCupboard = new CupboardBuilder(cupboard()).useAnnotations().build();
        annotatedCupboard.withDatabase(db).upgradeTables();
        // do migration work
    }

    public static void register(){
        if (!cupboard().isRegisteredEntity(UserMessage.class)){
            cupboard().register(UserMessage.class);
        }
        if (!cupboard().isRegisteredEntity(UserFriend.class)){
            cupboard().register(UserFriend.class);
        }
        if (!cupboard().isRegisteredEntity(UserContact.class)){
            cupboard().register(UserContact.class);
        }
        if (!cupboard().isRegisteredEntity(UserProfile.class)){
            cupboard().register(UserProfile.class);
        }

        if(!cupboard().isRegisteredEntity(UserSecretKey.class)){
            cupboard().register(UserSecretKey.class);
        }
    }
}
